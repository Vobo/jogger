FROM php:7.2-fpm

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update \
    && apt-get install -y --no-install-recommends apt-utils \
    && apt-get install -y --no-install-recommends \
    libicu-dev \
    libxml2 \
    unzip \
    curl && apt-get clean -y && rm -rf /var/lib/apt/lists/*

RUN docker-php-ext-configure intl && docker-php-ext-install -j4 intl
RUN docker-php-ext-install -j4 bcmath mbstring

RUN apt-get update \
    && buildDeps="libmemcached-dev zlib1g-dev" \
    && doNotUninstall="libmemcached11 libmemcachedutil2" \
    && apt-get install -y git openssh-client $buildDeps --no-install-recommends \
    && rm -r /var/lib/apt/lists/* \
    && docker-php-source extract \
    && git clone --branch php7 https://github.com/php-memcached-dev/php-memcached /usr/src/php/ext/memcached/ \
    && docker-php-ext-install -j4 memcached pdo pdo_mysql zip \
    && docker-php-source delete \
    && apt-mark manual $doNotUninstall \
    && apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false $buildDeps

# Install composer dependencies
RUN apt-get update && apt-get install -y bash git bash --no-install-recommends && rm -r /var/lib/apt/lists/*

RUN mkdir -p --mode=0777 /var/www/.composer/cache && chmod a+rwx /var/www/.composer/cache
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && php -r "if (hash_file('SHA384', 'composer-setup.php') === '93b54496392c062774670ac18b134c3b3a95e5a5e5c8f1a9f115f203b75bf9a129d5daa8ba6a13e2cc8a1da0806388a8') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" \
    && php composer-setup.php \
    && php -r "unlink('composer-setup.php');" \
    && mv composer.phar /usr/bin/composer

# Install xDebug
RUN pecl install xdebug && docker-php-ext-enable xdebug

# Copy xdebug configration for remote debugging
COPY ./xdebug.ini $PHP_INI_DIR/conf.d/xdebug.ini
ADD ./php_dev.ini /usr/local/etc/php/php.ini

COPY entrypoint.sh /
RUN chmod a+x /entrypoint.sh || true

VOLUME /srv/www/service
WORKDIR /srv/www/service

EXPOSE 9000

ENTRYPOINT ["/entrypoint.sh"]
