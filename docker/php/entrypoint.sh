#!/bin/bash

echo "Startup"
cd /srv/www/service && php bin/console cache:clear --no-debug --env=prod
echo "- cache cleaned"

[ -d ./vendor ] && chown -Rh www-data:www-data ./vendor
chown -Rh www-data:www-data /srv/www/service/var
chmod -R o+w /srv/www/service/var
chmod -R a+x /srv/www/service/bin/console
echo "- permissions set"

exec "php-fpm"
