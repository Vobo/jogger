<?php declare(strict_types=1);

namespace Tests\App\Service\Voter\OwnerResolver;

use App\Entity\User;
use App\Service\Voter\OwnerResolver\OwnerResolverInterface;
use App\Service\Voter\OwnerResolver\UserOwnerResolver;
use PHPUnit\Framework\TestCase;

class UserOwnerResolverTest extends TestCase
{
    /**
     * @var OwnerResolverInterface
     */
    private $ownerResolver;

    public function setUp()
    {
        $this->ownerResolver = new UserOwnerResolver();
    }

    /**
     * @dataProvider ownerDataProvider
     */
    public function testIsOwner(User $owner, User $target, bool $expectedResult)
    {
        $result = $this->ownerResolver->isOwner($owner, $target);

        $this->assertEquals($expectedResult, $result, sprintf(
            'owner: %s role, target: %s role, same instance: %s',
            $owner->getMainRole(),
            $target->getMainRole(),
            $owner === $target ? 'true' : 'false'
        ));
    }

    /**
     * Data provider
     */
    public function ownerDataProvider(): array
    {
        $admin1 = (new User())->setRoles(['ROLE_ADMIN']);
        $admin2 = (new User())->setRoles(['ROLE_ADMIN']);
        $manager1 = (new User())->setRoles(['ROLE_MANAGER']);
        $manager2 = (new User())->setRoles(['ROLE_MANAGER']);
        $customer1 = (new User())->setRoles(['ROLE_CUSTOMER']);
        $customer2 = (new User())->setRoles(['ROLE_CUSTOMER']);

        return [
            [$admin1, $admin1, true],
            [$admin1, $admin2, true],
            [$admin1, $manager1, true],
            [$admin1, $manager2, true],
            [$admin1, $customer1, true],
            [$admin1, $customer2, true],

            [$manager1, $admin1, false],
            [$manager1, $admin2, false],
            [$manager1, $manager1, true],
            [$manager1, $manager2, false],
            [$manager1, $customer1, true],
            [$manager1, $customer2, true],

            [$customer1, $admin1, false],
            [$customer1, $admin2, false],
            [$customer1, $manager1, false],
            [$customer1, $manager2, false],
            [$customer1, $customer1, true],
            [$customer1, $customer2, false],
        ];
    }
}
