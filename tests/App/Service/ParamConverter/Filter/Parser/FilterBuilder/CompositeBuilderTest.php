<?php declare(strict_types=1);

namespace Tests\App\Service\ParamConverter\Filter\Parser;

use App\Service\ParamConverter\Filter\Parser\FilterBuilder\CompositeBuilder;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\Query\Expr\Andx;
use Doctrine\ORM\Query\Expr\Comparison;
use Doctrine\ORM\Query\Expr\Orx;
use PHPUnit\Framework\TestCase;

class CompositeBuilderTest extends TestCase
{
    /**
     * @var CompositeBuilder
     */
    private $builder;

    public function setUp()
    {
        $this->builder = new CompositeBuilder();
    }

    public function testCanCreateEmptyFilter()
    {
        $filter = $this->builder->build();

        $this->assertInstanceOf(Andx::class, $filter);
    }

    public function testCanChangeTypeToAnd()
    {
        $filter = $this->builder
            ->setAnd()
            ->build();

        $this->assertInstanceOf(Andx::class, $filter);
    }

    public function testCanChangeTypeToOr()
    {
        $filter = $this->builder
            ->setOr()
            ->build();

        $this->assertInstanceOf(Orx::class, $filter);
    }

    /**
     * @expectedException \App\Service\ParamConverter\Filter\Parser\Exception\BuilderException
     */
    public function testCanChangeTypeBetweenTypes1()
    {
        $this->builder
            ->setOr()
            ->setAnd();
    }

    /**
     * @expectedException \App\Service\ParamConverter\Filter\Parser\Exception\BuilderException
     */
    public function testCanChangeTypeBetweenTypes2()
    {
        $this->builder
            ->setAnd()
            ->setOr();
    }

    public function testChangingTypeToSameAllowed1()
    {
        for ($i=0; $i<2; ++$i) {
            $this->builder->setAnd();
            $filter = $this->builder->build();

            $this->assertInstanceOf(Andx::class, $filter);
        }
    }

    public function testChangingTypeToSameAllowed2()
    {
        for ($i=0; $i<2; ++$i) {
            $this->builder->setOr();
            $filter = $this->builder->build();

            $this->assertInstanceOf(Orx::class, $filter);
        }
    }

    public function testCanSetParent()
    {
        $builder = new CompositeBuilder($this->builder);

        $this->assertEquals($this->builder, $builder->getParent());
    }

    /**
     * @dataProvider addThisDataProvider
     */
    public function testCanAdd($child)
    {
        /** @var Andx $filter */
        $filter = $this->builder
            ->addChild($child)
            ->build();

        $this->assertCount(1, $filter->getParts());
        $this->assertEquals($child, $filter->getParts()[0]);
    }

    /**
     * @dataProvider emptyCompositeDataProvider
     */
    public function testAddSkipsEmptyComposites(Expr\Composite $child)
    {
        $filter = $this->builder
            ->addChild($child)
            ->build();

        $this->assertCount(0, $filter->getParts());
        $this->assertInstanceOf(Andx::class, $filter);

        $filter = $this->builder
            ->setOr()
            ->build();

        $this->assertCount(0, $filter->getParts());
        $this->assertInstanceOf(Orx::class, $filter);
    }

    /**
     * Data provider
     */
    public function emptyCompositeDataProvider(): array
    {
        return [
            [new Andx()],
            [new Orx()],
        ];
    }

    /**
     * Data provider
     */
    public function addThisDataProvider(): array
    {
        $comparison = new Comparison('a', Comparison::EQ, 'b');

        return [
            [new Andx([$comparison])],
            [new Orx([$comparison])],
            [new Comparison('a', Comparison::EQ, 'b')],
        ];
    }

    /**
     * @expectedException \App\Service\ParamConverter\Filter\Parser\Exception\BuilderException
     * @dataProvider cannotAddThisDataProvider
     */
    public function testCannotAdd($child)
    {
        $this->builder->addChild($child);
    }

    /**
     * Data provider
     */
    public function cannotAddThisDataProvider(): array
    {
        $expr = new Expr();

        return [
            [new \stdClass()],
            [$expr->asc('')],
            [$expr->avg('')],
            [$expr->diff(1, 5)],
            ['string'],
            [null],
        ];
    }
}
