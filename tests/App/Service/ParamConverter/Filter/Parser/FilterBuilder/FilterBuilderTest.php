<?php declare(strict_types=1);

namespace Tests\App\Service\ParamConverter\Filter\Parser\FilterBuilder;

use App\Manager\Query\Filter;
use App\Service\ParamConverter\Filter\Parser\FilterBuilder\FilterBuilder;
use Doctrine\ORM\Query\Expr\Andx;
use Doctrine\ORM\Query\Expr\Orx;
use Doctrine\ORM\Query\Expr\Comparison;
use PHPUnit\Framework\TestCase;

class FilterBuilderTest extends TestCase
{
    /**
     * @var FilterBuilder
     */
    private $builder;

    public function setUp()
    {
        $this->builder = new FilterBuilder();
    }

    /**
     * @expectedException \App\Service\ParamConverter\Filter\Parser\Exception\BuilderException
     */
    public function testCannotLeaveLastContext1()
    {
        $this->builder->leaveContext();
    }

    /**
     * @expectedException \App\Service\ParamConverter\Filter\Parser\Exception\BuilderException
     */
    public function testCannotLeaveLastContext2()
    {
        $this->builder
            ->enterContext()
            ->leaveContext()
            ->leaveContext();
    }

    public function testAddComparisons()
    {
        $filter = $this->builder
            ->setOr()
            ->add(new Comparison('koza', '=', 7))
            ->enterContext()
                ->setAnd()
                ->add(new Comparison('rower', '<=', 'mememe'))
                ->add(new Comparison('rower', '>=', 'me'))
            ->leaveContext()
            ->add(new Comparison('gimli1', '!=', 'gimli2'))
            ->enterContext()
                ->add(new Comparison('dziadek', '=', 'swieta'))
            ->leaveContext()
            ->enterContext()
                ->setOr()
            ->leaveContext()
            ->build();

        $children = $filter->getWhere()->getParts();

        $this->assertInstanceOf(Filter::class, $filter);
        $this->assertInstanceOf(Andx::class, $filter->getWhere());
        $this->assertCount(1, $children);

        /** @var Orx $filter */
        $filter = $children[0];
        $children = $filter->getParts();

        $this->assertInstanceOf(Orx::class, $filter);
        $this->assertCount(4, $children);

        /** @var Comparison $child0 */
        $child0 = $children[0];
        $this->assertInstanceOf(Comparison::class, $child0);
        $this->assertEquals('koza', $child0->getLeftExpr());
        $this->assertEquals(7, $child0->getRightExpr());

        /** @var Andx $child1 */
        $child1 = $children[1];
        $this->assertInstanceOf(Andx::class, $child1);
        $this->assertCount(2, $child1->getParts());
        $this->assertInstanceOf(Comparison::class, $child1->getParts()[0]);
        $this->assertInstanceOf(Comparison::class, $child1->getParts()[1]);

        /** @var Comparison $child2 */
        $child2 = $children[2];
        $this->assertInstanceOf(Comparison::class, $child2);
        $this->assertEquals('gimli1', $child2->getLeftExpr());
        $this->assertEquals('gimli2', $child2->getRightExpr());

        /** @var Andx $child3 */
        $child3 = $children[3];
        $this->assertInstanceOf(Andx::class, $child3);
        $this->assertCount(1, $child3->getParts());
    }
}
