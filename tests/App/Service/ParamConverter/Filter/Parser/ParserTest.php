<?php declare(strict_types=1);

namespace Tests\App\Service\ParamConverter\Filter\Parser;

use App\Manager\Query\Filter;
use App\Service\ParamConverter\Filter\Config\Column;
use App\Service\ParamConverter\Filter\Config\ColumnRegistry;
use App\Service\ParamConverter\Filter\Parser\LogicalOperation\AndOperation;
use App\Service\ParamConverter\Filter\Parser\LogicalOperation\LogicalOperationRegistry;
use App\Service\ParamConverter\Filter\Parser\LogicalOperation\OrOperation;
use App\Service\ParamConverter\Filter\Parser\Operator\ComparisonOperator;
use App\Service\ParamConverter\Filter\Parser\Operator\OperatorRegistry;
use App\Service\ParamConverter\Filter\Parser\Parser;
use PHPUnit\Framework\TestCase;

class ParserTest extends TestCase
{
    /**
     * @var Parser
     */
    private $parser;

    /**
     * @var ColumnRegistry
     */
    private $columnConfigRegistry;

    public function setUp()
    {
        $operatorRegistry = new OperatorRegistry([
            new ComparisonOperator('eq' ,'='),
            new ComparisonOperator('neq' ,'<>'),
            new ComparisonOperator('gt' ,'>'),
            new ComparisonOperator('gte' ,'>='),
            new ComparisonOperator('lt' ,'<'),
            new ComparisonOperator('lte' ,'<='),
        ]);

        $logicalOperationsRegistry = new LogicalOperationRegistry([
            new AndOperation(),
            new OrOperation(),
        ]);

        $this->parser = new Parser($operatorRegistry, $logicalOperationsRegistry);

        $this->columnConfigRegistry = new ColumnRegistry([
            new Column('koza', 'a.koza', true,false),
            new Column('meme', 'm.meme', true,false),
            new Column('a', 'meme', true,false),
            new Column('b', 'meme', true,false),
            new Column('c', 'meme', true,false),
            new Column('date', 'date', true,false),
            new Column('ab', 'ba', true,false),
            new Column('abc', 'cba', false,true),
        ]);
    }

    /**
     * @dataProvider validStringsDataProvider
     */
    public function testCanParse($string)
    {
        $result = $this->parser->parse($this->columnConfigRegistry, $string);

        $this->assertInstanceOf(Filter::class, $result);
    }

    /**
     * Data provider
     */
    public function validStringsDataProvider(): array
    {
        return [
            [''],
            ['   '],
            ['(koza eq 100)'],
            ['(meme neq 15) or (meme neq 13)'],
            ['((meme neq 15) or (meme neq 13))'],
            ['((a eq 1) or (a eq 2)  or  (a eq 3)) and ((b neq 4) or (a neq 6)) and (c lte 10)'],
            ['(koza eq 100) and ((a gt 100) or (a lte 200))'],
            ['(b EQ 100) AND (b LT 20)'],
            ['(((a lte 20)))'],
            ['(date eq "2018-06-05")'],
            ["(date eq '2018-06-05')"],
            ["(date eq 'meme \"or\" not meme')"],
            ["(date eq \"meme  ' meme\")"],
            ["(date eq '')"],
            ["(date eq ' ')"],
        ];
    }

    /**
     * @expectedException \App\Service\ParamConverter\Filter\Parser\Exception\ParserException
     * @dataProvider invalidStringsDataProvider
     */
    public function testThrowsException($string)
    {
        $this->parser->parse($this->columnConfigRegistry, $string);
    }

    public function invalidStringsDataProvider(): array
    {
        return [
            ['a OR 100'], // Missing brackets
            ['(a or 100'], // Missing closing bracket
            ['(a eq 100) or (b eq 100) and (c eq 100)'],// Cannot change logical operation without encapsulation everything in brackets
            ['(((a eq 100))'], // Not all brackets closed
            ['(a_b eq 100)'], // Invalid variable name
            ['(a-b eq 100)'], // Invalid variable name
            ['(ab:cd eq 100)'], // Invalid variable name
            ['(ab cos 100)'], // Invalid operator
            ['(ab eq 100) cos (ab eq 200)'], // Invalid logical operation
            ['(ab eq "100\')'], // Missing closing quote
            ['(ab eq \'100")'], // Missing closing quote
            ['(ab eq "'], // Missing closing quote
            ['(abc eq 100)'], // Column cannot be filtered by
        ];
    }

    /**
     * @dataProvider extractDataProvider
     */
    public function testCanExtract($value, $expectedResult)
    {
        $string = "(ab eq $value)";
        $result = $this->parser->parse($this->columnConfigRegistry, $string);
        $params = $result->getParameters();

        $this->assertInstanceOf(Filter::class, $result);
        $this->assertCount(1, $params);
        $this->assertEquals($expectedResult,reset($params));
    }

    /**
     * Data provider
     */
    public function extractDataProvider(): array
    {
        return [
            ['\'a-b-c_def huehue :nope "\'', 'a-b-c_def huehue :nope "'],
            [-20, '-20'],
            [15.5, '15.5'],
            [-15.123, '-15.123'],
            ['koza', 'koza'],
        ];
    }
}
