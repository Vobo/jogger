<?php declare(strict_types=1);

namespace Tests\App\Helper;

use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PropertyAccess\PropertyAccess;

trait RequestTrait
{
    /**
     * @param array $options An array of options to pass to the createKernel method
     * @param array $server  An array of server parameters
     *
     * @return Client A Client instance
     */
    abstract protected static function createClient(array $options = array(), array $server = array());

    /**
     * Sends a request, authenticated as $user, with JSON $data
     *
     * @param string $method
     * @param string $url
     * @param string|null $user
     * @param array $data
     * @return Response
     */
    protected function sendRequest(string $method, string $url, string $user = null, array $data = null)
    {
        $headers = [];

        if ($user) {
            $headers['HTTP_Authorization'] = 'Bearer token-'.$user;
        }

        if (null !== $data) {
            $headers['CONTENT_TYPE'] = 'application/json';
            $data = json_encode($data);
        }

        $client = self::createClient();
        $client->request(
            $method,
            $url,
            [],
            [],
            $headers,
            $data
        );

        return $client->getResponse();
    }

    /**
     * @param Response $response
     * @return array
     */
    protected function deserialize(Response $response): array
    {
        $page = json_decode($response->getContent(), true);

        if (null === $page || json_last_error()) {
            throw new \Exception('Could not decode response: '.$response);
        }

        return $page;
    }

    /**
     * @param array $expected ['path' => 'value', ...]
     * @param Response $response
     */
    protected function assertResponseContains(array $expected, Response $response): void
    {
        $response = $this->deserialize($response);
        $propertyAccessor = PropertyAccess::createPropertyAccessor();

        foreach ($expected as $path => $expectedValue) {
            $value = $propertyAccessor->getValue($response, $path);
            Assert::assertEquals($expectedValue, $value, sprintf(
                'Invalid value in %s, expected %s, got %s',
                $path,
                $expectedValue,
                $value
            ));
        }
    }
}
