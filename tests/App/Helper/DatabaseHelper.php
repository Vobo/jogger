<?php declare(strict_types=1);

namespace Tests\App\Helper;

use Doctrine\ORM\EntityManagerInterface;
use Nelmio\Alice\Loader\NativeLoader;
use PHPUnit\Framework\Assert;
use Psr\Container\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\HttpKernel\KernelInterface;

trait DatabaseHelper
{
    /**
     * @var ContainerInterface
     */
    protected static $container;

    /**
     * Boots the Kernel for this test.
     *
     * @return KernelInterface A KernelInterface instance
     */
    abstract protected static function bootKernel(array $options = array());

    /**
     * Bootstraps empty db, then loads fixtures
     */
    protected static function prepareDb(array $files)
    {
        static::runCommand([
            'command' => 'doctrine:schema:drop',
            '--full-database' => true,
            '--no-interaction' => true,
            '--force' => true,
        ]);
        static::runCommand([
            'command' => 'doctrine:migrations:migrate',
            '--no-interaction' => true,
        ]);
        static::loadFixtures($files);
    }

    /**
     * @param string $class
     * @param int $id
     */
    protected function assertEntityExists(string $class, int $id): void
    {
        $object = $this->getEntityManager()->find($class, $id);

        if (!$object) {
            Assert::fail(sprintf('Could not find entity %s with id %d', $class, $id));
        }
    }

    /**
     * @param string $class
     * @param int $id
     * @return null|object
     */
    protected function fetchEntity(string $class, int $id): ?object
    {
        return $this->getEntityManager()->find($class, $id);
    }

    /**
     * @param string $class
     * @param int $id
     */
    protected function assertEntityDoesNotExists(string $class, int $id): void
    {
        $object = $this->getEntityManager()->find($class, $id);

        if ($object) {
            Assert::fail(sprintf('Entity %s with id %d exists', $class, $id));
        }
    }

    /**
     * @param array $input
     */
    private static function runCommand(array $input): void
    {
        $kernel = static::bootKernel();
        $application = new Application($kernel);
        $application->setAutoExit(false);
        $application->run(new ArrayInput($input), new NullOutput());
    }

    /**
     * @param string[] $files
     */
    private static function loadFixtures(array $files): void
    {
        foreach ($files as &$file) {
            $file = __DIR__ . '/../../fixtures/' .$file;
        }

        $entityManager = static::getEntityManager();
        $loader = new NativeLoader();
        $objectSet = $loader->loadFiles($files);

        foreach ($objectSet->getObjects() as $object) {
            $entityManager->persist($object);
        }

        $entityManager->flush();
    }

    /**
     * @return EntityManagerInterface
     */
    private static function getEntityManager(): EntityManagerInterface
    {
        return static::$container->get(EntityManagerInterface::class);
    }
}
