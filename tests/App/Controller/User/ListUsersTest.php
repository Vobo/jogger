<?php declare(strict_types=1);

namespace Tests\App\Controller\User;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Tests\App\Helper\DatabaseHelper;
use Tests\App\Helper\RequestTrait;

class ListUsersTest extends WebTestCase
{
    use DatabaseHelper;
    use RequestTrait;

    public static function setUpBeforeClass()
    {
        static::prepareDb([
            'setup.yaml',
            'users.yaml',
        ]);
    }

    /**
     * @dataProvider listUsersDataProvider
     */
    public function testListUsers(?string $user, int $expectedStatusCode, array $responseContains = null)
    {
        $response = $this->sendRequest('GET', '/api/user?limit=1', $user);

        $this->assertEquals($expectedStatusCode, $response->getStatusCode());

        if ($responseContains) {
            $this->assertResponseContains($responseContains, $response);
        }
    }

    /**
     * Data provider
     */
    public function listUsersDataProvider(): array
    {
        $allUsers = [
            '[page]' => 1,
            '[pages]' => 64,
            '[limit]' => 1,
            '[users][0][id]' => 1,
        ];

        return [
            [null, 401],
            ['user-1', 200, $allUsers],
            ['manager', 200, $allUsers],
            ['admin', 200, $allUsers]
        ];
    }

    /**
     * @dataProvider paginationTestDataProvider
     */
    public function testPagination($page, $limit, array $expectContains, int $expectedResults)
    {
        $url = sprintf('/api/user?page=%s&limit=%s', $page, $limit);
        $response = $this->sendRequest('GET', $url, 'manager');

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertResponseContains($expectContains, $response);
        $data = $this->deserialize($response);
        $this->assertCount($expectedResults, $data['users']);
    }

    /**
     * Data provider
     */
    public function paginationTestDataProvider(): array
    {
        $generator = function(int $page, int $pages, int $limit) {
            return [
                '[page]' => $page,
                '[pages]' => $pages,
                '[limit]' => $limit,
            ];
        };

        return [
            ['a', 'a', $generator(1, 7, 10), 10],
            [-10, -15, $generator(1, 7, 10), 10],
            [0, 0, $generator(1, 7, 10), 10],
            [1.5, 10.4, $generator(1, 7, 10), 10],
            ['2h', '6f', $generator(1, 7, 10), 10],
            [3, 11, $generator(3, 6, 11), 11],
            [7, 10, $generator(7, 7, 10), 4],
        ];
    }

    /**
     * @dataProvider filtersDataProvider
     */
    public function testFilters(string $user, string $filter, array $responseContains)
    {
        $response = $this->sendRequest('GET', '/api/user?limit=1&filter='.$filter, $user);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertResponseContains($responseContains, $response);
    }

    /**
     * Data provider
     */
    public function filtersDataProvider(): array
    {
        return [
            ['admin', '', ['[pages]' => 64]], // Return all
            ['admin', '(id lt 10)', ['[pages]' => 9]],
            ['admin', '(username has "admin")', ['[pages]' => 21]],
            ['admin', '(role has "ROLE_CUSTOMER")', ['[pages]' => 22]],
            ['admin', '((role has "ROLE_CUSTOMER") or (role has "ROLE_MANAGER"))', ['[pages]' => 43]],
        ];
    }
}
