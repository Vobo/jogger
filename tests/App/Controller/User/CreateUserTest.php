<?php declare(strict_types=1);

namespace Tests\App\Controller\User;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Tests\App\Helper\DatabaseHelper;
use Tests\App\Helper\RequestTrait;

class CreateUserTest extends WebTestCase
{
    use DatabaseHelper;
    use RequestTrait;

    public function setUp()
    {
        static::prepareDb([
            'setup.yaml',
        ]);
    }

    /**
     * @dataProvider createUserDataProvider
     */
    public function testCreateUser(?string $user, string $role, string $expectedRole)
    {
        $response = $this->sendRequest('POST', '/api/user', $user, [
            'username' => 'koza',
            'email' => 'koza@test.com',
            'password' => 'cojapacze',
            'role' => $role,
        ]);

        $this->assertEquals(201, $response->getStatusCode());
        $this->assertResponseContains([
            '[id]' => 5,
            '[username]' => 'koza',
            '[email]' => 'koza@test.com',
            '[role]' => $expectedRole,
        ], $response);
        $this->assertEntityExists(User::class, 5);
    }

    /**
     * Data provider
     */
    public function createUserDataProvider(): array
    {
        return [
            [null, 'watwatwat', 'ROLE_CUSTOMER'], // Can't set role during creation
            [null, 'ROLE_ADMIN', 'ROLE_CUSTOMER'], // Can't set role during creation

            ['user-1', 'watwatwat', 'ROLE_CUSTOMER'], // Can't set role during creation
            ['user-1', 'ROLE_ADMIN', 'ROLE_CUSTOMER'], // Can't set role during creation

            ['manager', 'ROLE_CUSTOMER', 'ROLE_CUSTOMER'], // Manager can set role during creation
            ['manager', 'ROLE_MANAGER', 'ROLE_MANAGER'], // Manager can set role during creation

            ['admin', 'ROLE_CUSTOMER', 'ROLE_CUSTOMER'], // Admin can set role during creation
            ['admin', 'ROLE_MANAGER', 'ROLE_MANAGER'], // Admin can set role during creation
            ['admin', 'ROLE_ADMIN', 'ROLE_ADMIN'], // Admin can set role during creation
        ];
    }

    /**
     * @dataProvider missingPropertiesDataProvider
     */
    public function testRequestMissingProperties(?string $user, bool $shouldValidateRole)
    {
        $response = $this->sendRequest('POST', '/api/user', $user, []);
        $content = $this->deserialize($response);
        $responseShouldContain = [
            '[0][property_path]' => 'username',
            '[1][property_path]' => 'email',
            '[2][property_path]' => 'password',
        ];

        if ($shouldValidateRole) {
            $responseShouldContain['[3][property_path]'] = 'role';
        }

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertResponseContains($responseShouldContain, $response);
        $this->assertCount($shouldValidateRole ? 4 : 3, $content);
    }

    /**
     * Data provider
     */
    public function missingPropertiesDataProvider(): array
    {
        return [
            ['admin', true],
            ['manager', true],
            ['user-1', false],
            [null, false],
        ];
    }

    /**
     * @dataProvider usernameValidationDataProvider
     */
    public function testUsernameValidation(?string $user, $username, bool $shouldPass)
    {
        $response = $this->sendRequest('POST', '/api/user', $user, [
            'username' => $username,
            'email' => 'koza@test.com',
            'password' => 'cojapacze',
            'role' => 'ROLE_CUSTOMER',
        ]);
        $content = $this->deserialize($response);

        if (!$shouldPass) {
            $this->assertEquals(400, $response->getStatusCode());
            $this->assertCount(1, $content);
            $this->assertResponseContains(['[0][property_path]' => 'username'], $response);
        } else {
            $this->assertEquals(201, $response->getStatusCode());
            $this->assertResponseContains([
                '[username]' => $username,
            ], $response);
        }
    }

    /**
     * Data provider
     */
    public function usernameValidationDataProvider(): \Generator
    {
        foreach ([null, 'admin', 'manager', 'user-1'] as $user) {
            yield [$user, null, false]; // Invalid value
            yield [$user, -1, false]; // Too short (after casting to string)
            yield [$user, '0', false]; // Too short
            yield [$user, 'aa', false]; // Too short
            yield [$user, 'aaa', true]; // Ok
            yield [$user, 'manager', false]; // Already taken
            yield [$user, 'Dbt0zs20vVHmpeOGZfME6QKvKHWnukW9ISGkeHrTyhWAN21yx4icvS9NTOkpA5VkCDWZTfWzyMXI2nid1CYUpGWEHh9XAn4JsXZ0XaRUPTMkppXJq9hvyYklEiQ14zrOUi9wwxPmwVt1uJBb8UIorTI825gvUoJNPQQFo4bPOzGZLD64Ou8M', true]; // Ok, 180 chars
            yield [$user, 'Dbt0zs20vVHmpeOGZfME6QKvKHWnukW9ISGkeHrTyhWAN21yx4icvS9NTOkpA5VkCDWZTfWzyMXI2nid1CYUpGWEHh9XAn4JsXZ0XaRUPTMkppXJq9hvyYklEiQ14zrOUi9wwxPmwVt1uJBb8UIorTI825gvUoJNPQQFo4bPOzGZLD64Ou8M1', false]; // Too long, 181 chars
        }
    }

    /**
     * @dataProvider emailValidationDataProvider
     */
    public function testEmailValidation(?string $user, $email, bool $shouldPass)
    {
        $response = $this->sendRequest('POST', '/api/user', $user, [
            'username' => 'koza',
            'email' => $email,
            'password' => 'cojapacze',
            'role' => 'ROLE_CUSTOMER',
        ]);
        $content = $this->deserialize($response);

        if (!$shouldPass) {
            $this->assertEquals(400, $response->getStatusCode());
            $this->assertCount(1, $content);
            $this->assertResponseContains(['[0][property_path]' => 'email'], $response);
        } else {
            $this->assertEquals(201, $response->getStatusCode());
            $this->assertResponseContains([
                '[email]' => $email,
            ], $response);
        }
    }

    /**
     * Data provider
     */
    public function emailValidationDataProvider(): \Generator
    {
        foreach ([null, 'admin', 'manager', 'user-1'] as $user) {
            yield [$user, null, false]; // Invalid value
            yield [$user, -1, false]; // Not email
            yield [$user, '0', false]; // Not email
            yield [$user, 'aa', false]; // Not email
            yield [$user, 'aaa', false]; // Not email
            yield [$user, 'manager@test.com', false]; // Already taken
            yield [$user, 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa@test.com', true]; // Ok, 180 chars
            yield [$user, 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa@test.com', false]; // Too long, 181 chars
            yield [$user, 'aa+test!#$%&\'*+-/=?^_`{@test.com', true]; // Some special chars, still valid
            yield [$user, 'nope@test.com.', false]; // Some special chars, still valid
        }
    }

    /**
     * @dataProvider passwordValidationDataProvider
     */
    public function testPasswordValidation(?string $user, $password, bool $shouldPass)
    {
        $response = $this->sendRequest('POST', '/api/user', $user, [
            'username' => 'koza',
            'email' => 'test@test.com',
            'password' => $password,
            'role' => 'ROLE_CUSTOMER',
        ]);
        $content = $this->deserialize($response);

        if (!$shouldPass) {
            $this->assertEquals(400, $response->getStatusCode());
            $this->assertCount(1, $content);
            $this->assertResponseContains(['[0][property_path]' => 'password'], $response);
        } else {
            $this->assertEquals(201, $response->getStatusCode());
        }
    }

    /**
     * Data provider
     */
    public function passwordValidationDataProvider(): \Generator
    {
        foreach ([null, 'admin', 'manager', 'user-1'] as $user) {
            yield [$user, null, false]; // Invalid value
            yield [$user, -1, false]; // Too short (after casting to string)
            yield [$user, '0', false]; // Too shory
            yield [$user, 'aaa', false]; // Too shory
            yield [$user, -1.345, true]; // Fine, 6 chars
            yield [$user, 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa@test.com', true]; // 72 chars, bcrypt max
            yield [$user, 'aa+test!#$%&\'*+-/=?^_`{@test', true]; // Some special chars, still valid
            yield [$user, '123456', true]; // Digits only
            yield [$user, '!@#$%^', true]; // Special chars only
        }
    }

    /**
     * @dataProvider roleValidationDataProvider
     */
    public function testRoleValidation(?string $user, $role, bool $shouldPass, string $expectedRole = null)
    {
        $response = $this->sendRequest('POST', '/api/user', $user, [
            'username' => 'koza',
            'email' => 'test@test.com',
            'password' => 'abcdef',
            'role' => $role,
        ]);
        $content = $this->deserialize($response);

        if (!$shouldPass) {
            $this->assertEquals(400, $response->getStatusCode());
            $this->assertCount(1, $content);
            $this->assertResponseContains(['[0][property_path]' => 'role'], $response);
        } else {
            $this->assertEquals(201, $response->getStatusCode());
            $this->assertResponseContains(['[role]' => $expectedRole], $response);
        }
    }

    /**
     * Data provider
     */
    public function roleValidationDataProvider(): \Generator
    {
        // Always valid, since 'role' property is not available for these gents
        foreach ([null, 'user-1'] as $user) {
            yield [$user, null, true, 'ROLE_CUSTOMER'];
            yield [$user, 'd21', true, 'ROLE_CUSTOMER'];
            yield [$user, 'ROLE_CUSTOMER', true, 'ROLE_CUSTOMER'];
            yield [$user, 'ROLE_MANAGER', true, 'ROLE_CUSTOMER'];
            yield [$user, 'ROLE_ADMIN', true, 'ROLE_CUSTOMER'];
        }

        foreach (['admin', 'manager'] as $user) {
            yield [$user, null, false]; // Invalid value
            yield [$user, '', false]; // Special chars only
            yield [$user, -1, false]; // Invalid value
            yield [$user, 'aaa', false]; // Invalid value
            yield [$user, 'ROLE_CUSTOMER', true, 'ROLE_CUSTOMER']; // Fine
            yield [$user, 'ROLE_MANAGER', true, 'ROLE_MANAGER']; // Fine
        }

        yield ['manager', 'ROLE_ADMIN', false]; // Only admin can create other admins
        yield ['admin', 'ROLE_ADMIN', true, 'ROLE_ADMIN']; // Only admin can create other admins
    }
}
