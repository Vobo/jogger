<?php declare(strict_types=1);

namespace Tests\App\Controller\User;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Tests\App\Helper\DatabaseHelper;
use Tests\App\Helper\RequestTrait;

class GetUserTest extends WebTestCase
{
    use DatabaseHelper;
    use RequestTrait;

    public static function setUpBeforeClass()
    {
        static::prepareDb([
            'setup.yaml',
        ]);
    }

    /**
     * @dataProvider getUserDataProvider
     */
    public function testGetUser(?string $user, int $userId, int $expectedStatusCode, array $expectedContent = null)
    {
        $response = $this->sendRequest('GET', '/api/user/'.$userId, $user);
        $this->assertEquals($expectedStatusCode, $response->getStatusCode());

        if ($expectedContent) {
            $this->assertResponseContains($expectedContent, $response);
        }
    }

    /**
     * Data provider
     */
    public function getUserDataProvider(): array
    {
        $content = [
            '[id]' => 3,
            '[username]' => 'user_1',
            '[email]' => 'user_1@test.com',
            '[role]' => 'ROLE_CUSTOMER',
        ];

        return [
            ['user-1', 1, 200, ['[id]' => 1]], // Can see admin
            ['user-1', 2, 200, ['[id]' => 2]], // Can see manager
            ['user-1', 3, 200, $content], // Can see yourself
            ['user-2', 3, 200], // Can see others
            ['user-2', 9, 404], // Actually not found
            ['user-2', 4, 200, ['[id]' => 4]], // Can see yourself
            ['admin', 1, 200, ['[id]' => 1]], // Can see admin
            ['admin', 2, 200, ['[id]' => 2]], // Can see manager
            ['admin', 4, 200, ['[id]' => 4]], // Can see user
            ['admin', 9, 404, ], // Actually not found
            ['manager', 1, 200, ['[id]' => 1]], // Can see admin
            ['manager', 2, 200, ['[id]' => 2]], // Can see manager
            ['manager', 3, 200, ['[id]' => 3]], // Can see user
            ['manager', 9, 404], // Actually not found
            [null, 1, 401],
            [null, 2, 401],
            [null, 3, 401],
            [null, 9, 401],
        ];
    }
}
