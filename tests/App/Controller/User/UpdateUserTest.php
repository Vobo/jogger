<?php declare(strict_types=1);

namespace Tests\App\Controller\User;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Tests\App\Helper\DatabaseHelper;
use Tests\App\Helper\RequestTrait;

class UpdateUserTest extends WebTestCase
{
    use DatabaseHelper;
    use RequestTrait;

    public function setUp()
    {
        static::prepareDb([
            'setup.yaml',
        ]);
    }

    /**
     * @dataProvider whoCanChangeWhoDataProvider
     */
    public function testWhoCanChangeWho(?string $user, int $userId, int $expectedStatusCode)
    {
        $response = $this->sendRequest('PUT', '/api/user/'.$userId, $user, [
            'username' => 'koza',
            'email' => 'koza@test.com',
        ]);

        $this->assertEquals($expectedStatusCode, $response->getStatusCode());
    }

    /**
     * Data provider
     */
    public function whoCanChangeWhoDataProvider(): array
    {
        return [
            [null, 1, 401],
            [null, 2, 401],
            [null, 3, 401],
            [null, 4, 401],

            ['user-1', 1, 404],
            ['user-1', 2, 404],
            ['user-1', 3, 200],
            ['user-1', 4, 404],

            ['user-2', 1, 404],
            ['user-2', 2, 404],
            ['user-2', 3, 404],
            ['user-2', 4, 200],

            ['manager', 1, 404],
            ['manager', 2, 200],
            ['manager', 3, 200],
            ['manager', 4, 200],

            ['admin', 1, 200],
            ['admin', 2, 200],
            ['admin', 3, 200],
            ['admin', 4, 200],
        ];
    }

    public function testUpdateUser()
    {
        $response = $this->sendRequest('PUT', '/api/user/1', 'admin', [
            'username' => 'test_user',
            'email' => 'test_user@test.com',
        ]);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertResponseContains([
            '[id]' => 1,
            '[username]' => 'test_user',
            '[email]' => 'test_user@test.com',
            '[role]' => 'ROLE_ADMIN'], $response
        );

        /** @var User $entity */
        $entity = $this->fetchEntity(User::class, 1);

        $this->assertEquals('test_user', $entity->getUsername());
        $this->assertEquals('test_user@test.com', $entity->getEmail());
        $this->assertContains('ROLE_ADMIN', $entity->getRoles());
    }

    /**
     * @dataProvider passwordDataProvider
     */
    public function testCannotChangePassword(?string $user, $password)
    {
        /** @var User $entity */
        $entity = $this->getEntityManager()->find(User::class, 1);
        $oldPassword = $entity->getPassword();

        $response = $this->sendRequest('PUT', '/api/user/3', $user, [
            'username' => 'koza',
            'email' => 'koza@test.com',
            'password' => $password,
        ]);

        $entity = $this->getEntityManager()->find(User::class, 1);
        $newPassword = $entity->getPassword();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals($oldPassword, $newPassword);
    }

    /**
     * Data provider
     */
    public function passwordDataProvider(): \Generator
    {
        foreach (['user-1', 'manager', 'admin'] as $user) {
            yield [$user, null];
            yield [$user, ''];
            yield [$user, -1.12];
            yield [$user, '1'];
            yield [$user, 'shouldBeValid'];
        }
    }

    /**
     * @dataProvider roleDataProvider
     */
    public function testCannotChangeRole(?string $user, $role)
    {
        /** @var User $entity */
        $entity = $this->getEntityManager()->find(User::class, 1);
        $oldRoles = $entity->getRoles();

        $response = $this->sendRequest('PUT', '/api/user/3', $user, [
            'username' => 'koza',
            'email' => 'koza@test.com',
            'role' => $role,
        ]);

        $entity = $this->getEntityManager()->find(User::class, 1);
        $newRoles = $entity->getRoles();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals($oldRoles, $newRoles);
    }

    /**
     * Data provider
     */
    public function roleDataProvider(): \Generator
    {
        foreach (['user-1', 'manager', 'admin'] as $user) {
            yield [$user, null];
            yield [$user, ''];
            yield [$user, -1.12];
            yield [$user, '1'];
            yield [$user, 'ROLE_CUSTOMER'];
            yield [$user, 'ROLE_MANAGER'];
            yield [$user, 'ROLE_ADMIN'];
        }
    }

    /**
     * @dataProvider usernameValidationDataProvider
     */
    public function testUsernameValidation(?string $user, $username, bool $shouldPass)
    {
        $response = $this->sendRequest('PUT', '/api/user/3', $user, [
            'username' => $username,
            'email' => 'yyy@test.com',
        ]);
        $content = $this->deserialize($response);

        if (!$shouldPass) {
            $this->assertEquals(400, $response->getStatusCode());
            $this->assertCount(1, $content);
            $this->assertResponseContains(['[0][property_path]' => 'username'], $response);
        } else {
            $this->assertEquals(200, $response->getStatusCode());
            $this->assertResponseContains([
                '[username]' => $username,
            ], $response);
        }
    }

    /**
     * Data provider
     */
    public function usernameValidationDataProvider(): \Generator
    {
        foreach (['admin', 'manager', 'user-1'] as $user) {
            yield [$user, null, false]; // Invalid value
            yield [$user, -1, false]; // Too short (after casting to string)
            yield [$user, '0', false]; // Too short
            yield [$user, 'aa', false]; // Too short
            yield [$user, 'aaa', true]; // Ok
            yield [$user, 'manager', false]; // Taken by someone else
            yield [$user, 'user_1', true]; // User's own username
            yield [$user, 'Dbt0zs20vVHmpeOGZfME6QKvKHWnukW9ISGkeHrTyhWAN21yx4icvS9NTOkpA5VkCDWZTfWzyMXI2nid1CYUpGWEHh9XAn4JsXZ0XaRUPTMkppXJq9hvyYklEiQ14zrOUi9wwxPmwVt1uJBb8UIorTI825gvUoJNPQQFo4bPOzGZLD64Ou8M', true]; // Ok, 180 chars
            yield [$user, 'Dbt0zs20vVHmpeOGZfME6QKvKHWnukW9ISGkeHrTyhWAN21yx4icvS9NTOkpA5VkCDWZTfWzyMXI2nid1CYUpGWEHh9XAn4JsXZ0XaRUPTMkppXJq9hvyYklEiQ14zrOUi9wwxPmwVt1uJBb8UIorTI825gvUoJNPQQFo4bPOzGZLD64Ou8M1', false]; // Too long, 181 chars
        }
    }

    /**
     * @dataProvider emailValidationDataProvider
     */
    public function testEmailValidation(?string $user, $email, bool $shouldPass)
    {
        $response = $this->sendRequest('PUT', '/api/user/3', $user, [
            'username' => 'koza',
            'email' => $email,
        ]);
        $content = $this->deserialize($response);

        if (!$shouldPass) {
            $this->assertEquals(400, $response->getStatusCode());
            $this->assertCount(1, $content);
            $this->assertResponseContains(['[0][property_path]' => 'email'], $response);
        } else {
            $this->assertEquals(200, $response->getStatusCode());
            $this->assertResponseContains([
                '[email]' => $email,
            ], $response);
        }
    }

    /**
     * Data provider
     */
    public function emailValidationDataProvider(): \Generator
    {
        foreach (['admin', 'manager', 'user-1'] as $user) {
            yield [$user, null, false]; // Invalid value
            yield [$user, -1, false]; // Not email
            yield [$user, '0', false]; // Not email
            yield [$user, 'aa', false]; // Not email
            yield [$user, 'aaa', false]; // Not email
            yield [$user, 'manager@test.com', false]; // Taken by someone else
            yield [$user, 'user_1@test.com', true]; // User's own email
            yield [$user, 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa@test.com', true]; // Ok, 180 chars
            yield [$user, 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa@test.com', false]; // Too long, 181 chars
            yield [$user, 'aa+test!#$%&\'*+-/=?^_`{@test.com', true]; // Some special chars, still valid
            yield [$user, 'nope@test.com.', false]; // Some special chars, still valid
        }
    }
}
