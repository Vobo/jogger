<?php declare(strict_types=1);

namespace Tests\App\Controller\User;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Tests\App\Helper\DatabaseHelper;
use Tests\App\Helper\RequestTrait;

class DeleteUserTest extends WebTestCase
{
    use DatabaseHelper;
    use RequestTrait;

    public function setUp()
    {
        static::prepareDb([
            'setup.yaml',
        ]);
    }

    /**
     * @dataProvider deleteUserDataProvider
     */
    public function testDeleteUser(?string $user, int $expectedStatusCode, bool $shouldExist, int $userId)
    {
        $response = $this->sendRequest('DELETE', '/api/user/'.$userId, $user);
        $this->assertEquals($expectedStatusCode, $response->getStatusCode());

        $shouldExist
            ? $this->assertEntityExists(User::class, $userId)
            : $this->assertEntityDoesNotExists(User::class, $userId);
    }

    /**
     * Data provider
     */
    public function deleteUserDataProvider(): array
    {
        return [
            ['user-1', 204, false, 3], // Can delete own user
            ['user-1', 404, true, 1], // But not others
            ['user-1', 404, true, 2], // But not others
            ['user-1', 404, true, 4], // But not others
            ['user-2', 404, false, 9], // Or someone that doesn't exist

            ['manager', 204, false, 2], // Manager can access all customers and managers
            ['manager', 204, false, 3], // Manager can access all customers and managers
            ['manager', 404, true, 1], // But not admins

            ['admin', 204, false, 3], // So can admin
            ['admin', 204, false, 1], // So can admin
            ['admin', 204, false, 2], // So can admin

            [null, 401, true, 1], // Unauthenticated users can't see anyone
            [null, 401, true, 2], // Unauthenticated users can't see anyone
            [null, 401, true, 3], // Unauthenticated users can't see anyone
            [null, 401, true, 4], // Unauthenticated users can't see anyone
        ];
    }
}
