<?php declare(strict_types=1);

namespace Tests\App\Controller\User;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Tests\App\Helper\DatabaseHelper;
use Tests\App\Helper\RequestTrait;

class StatisticsReportTest extends WebTestCase
{
    use DatabaseHelper;
    use RequestTrait;

    public static function setUpBeforeClass()
    {
        static::prepareDb([
            'setup.yaml',
            'activities.yaml',
            'more_activities.yaml',
        ]);
    }

    /**
     * @dataProvider accessStatisticsDataProvider
     */
    public function testListActivities(?string $user, int $page, int $expectedStatusCode, array $responseContains = null)
    {
        $response = $this->sendRequest('GET', '/api/report/statistics?limit=1&page='.$page, $user);

        $this->assertEquals($expectedStatusCode, $response->getStatusCode());

        if ($responseContains) {
            $this->assertResponseContains($responseContains, $response);
        }
    }

    /**
     * Data provider
     */
    public function accessStatisticsDataProvider(): array
    {
        $adminList = [
            '[page]' => 5,
            '[pages]' => 6,
            '[limit]' => 1,
            '[records][0][year]' => 2018,
            '[records][0][week]' => 33,
            '[records][0][averageDistance]' => 1501,
            '[records][0][averageTime]' => 151,
        ];

        $user1List = [
            '[page]' => 2,
            '[pages]' => 2,
            '[limit]' => 1,
            '[records][0][year]' => 2018,
            '[records][0][week]' => 41,
            '[records][0][averageDistance]' => 1002.5,
            '[records][0][averageTime]' => 752.5,
        ];

        $user2List = [
            '[page]' => 2,
            '[pages]' => 4,
            '[limit]' => 1,
            '[records][0][year]' => 2018,
            '[records][0][week]' => 37,
            '[records][0][averageDistance]' => 1521,
            '[records][0][averageTime]' => 151,
        ];

        return [
            [null, 1, 401],
            ['user-1', 2, 200, $user1List],
            ['user-2', 2, 200, $user2List],
            ['manager', 1, 403],
            ['admin', 5, 200, $adminList],
        ];
    }

    /**
     * @dataProvider paginationTestDataProvider
     */
    public function testPagination($page, $limit, array $expectContains, int $expectedResults)
    {
        $url = sprintf('/api/report/statistics?page=%s&limit=%s', $page, $limit);
        $response = $this->sendRequest('GET', $url, 'admin');

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertResponseContains($expectContains, $response);
        $data = $this->deserialize($response);
        $this->assertCount($expectedResults, $data['records']);
    }

    /**
     * Data provider
     */
    public function paginationTestDataProvider(): array
    {
        $generator = function(int $page, int $pages, int $limit) {
            return [
                '[page]' => $page,
                '[pages]' => $pages,
                '[limit]' => $limit,
            ];
        };

        return [
            ['a', 'a', $generator(1, 1, 10), 6],
            [-10, -15, $generator(1, 1, 10), 6],
            [0, 0, $generator(1, 1, 10), 6],
            [1.5, 10.4, $generator(1, 1, 10), 6],
            ['2h', '6f', $generator(1, 1, 10), 6],
            [3, 2, $generator(3, 3, 2), 2],
            [2, 4, $generator(2, 2, 4), 2],
        ];
    }

    /**
     * @dataProvider filtersDataProvider
     */
    public function testFilters(string $user, string $filter, array $responseContains)
    {
        $response = $this->sendRequest('GET', '/api/report/statistics?limit=1&filter='.$filter, $user);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertResponseContains($responseContains, $response);
    }

    /**
     * Data provider
     */
    public function filtersDataProvider(): array
    {
        return [
            ['admin', '', ['[pages]' => 6]], // Return all
            ['admin', '(user eq 1)', ['[pages]' => 0]],
            ['admin', '(user eq 4)', ['[pages]' => 4]],
            ['admin', '(user eq 3) or (user eq 4)', ['[pages]' => 6]],
            ['admin', '(year eq 2017)', ['[pages]' => 1]],
            ['admin', '(year gt 2017)', ['[pages]' => 5]],
            ['admin', '(week gte 41)', ['[pages]' => 3]],
            ['admin', '(distance gt 1501)', ['[pages]' => 3]],
            ['admin', '(time eq 151)', ['[pages]' => 3]],
            ['admin', '(latitude gt 100)', ['[pages]' => 5]],
            ['admin', '(longitude lt -100)', ['[pages]' => 5]],
            ['admin', '(temperature gte 50)', ['[pages]' => 3]],
            ['admin', '(windSpeed neq 36.5)', ['[pages]' => 2]],

            ['user-2', '', ['[pages]' => 4]], // Return all
            ['user-2', '(year eq 2017)', ['[pages]' => 1]],
            ['user-2', '(year gt 2017)', ['[pages]' => 3]],
            ['user-2', '(week gte 37)', ['[pages]' => 2]],
            ['user-2', '(distance neq 1501)', ['[pages]' => 1]],
            ['user-2', '(time neq 155)', ['[pages]' => 3]],
            ['user-2', '(latitude gt 123.321)', ['[pages]' => 1]],
            ['user-2', '(longitude lt -123.321)', ['[pages]' => 1]],
            ['user-2', '(temperature gt 12.2)', ['[pages]' => 2]],
            ['user-2', '(windSpeed neq 36.5)', ['[pages]' => 0]],
        ];
    }
}
