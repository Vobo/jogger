<?php declare(strict_types=1);

namespace Tests\App\Controller\Activity;

use App\Entity\Activity;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Tests\App\Helper\DatabaseHelper;
use Tests\App\Helper\RequestTrait;

class DeleteActivityTest extends WebTestCase
{
    use DatabaseHelper;
    use RequestTrait;

    public function setUp()
    {
        static::prepareDb([
            'setup.yaml',
            'activities.yaml',
        ]);
    }

    /**
     * @dataProvider deleteActivityDataProvider
     */
    public function testUserDeletesActivity(?string $user, int $expectedStatusCode, bool $shouldExist, int $activityId = 1)
    {
        $response = $this->sendRequest('DELETE', '/api/activity/'.$activityId, $user);
        $this->assertEquals($expectedStatusCode, $response->getStatusCode());

        $shouldExist
            ? $this->assertEntityExists(Activity::class, 1)
            : $this->assertEntityDoesNotExists(Activity::class, 1);
    }

    /**
     * Data provider
     */
    public function deleteActivityDataProvider(): array
    {
        return [
            ['user-1', 204, false], // Can delete own activity
            ['user-2', 404, true], // Can't delete someone elses
            ['user-2', 404, true, 9], // Or something that doesn't exist
            ['manager', 403, true], // Manager cannot access activities
            ['admin', 204, false], // But admin can and can delete for whoever he wants
            [null, 401, true], // Unauthenticated users can't access activities
        ];
    }
}
