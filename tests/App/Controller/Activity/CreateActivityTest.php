<?php declare(strict_types=1);

namespace Tests\App\Controller\User;

use App\Entity\Activity;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Tests\App\Helper\DatabaseHelper;
use Tests\App\Helper\RequestTrait;

class CreateActivityTest extends WebTestCase
{
    use DatabaseHelper;
    use RequestTrait;

    public function setUp()
    {
        static::prepareDb([
            'setup.yaml',
        ]);
    }

    /**
     * @dataProvider createActivityTestDataProvider
     */
    public function testCanCreateActivity(?string $user, int $expectedStatusCode)
    {
        $response = $this->sendRequest('POST', '/api/activity', $user, [
            'date' => '2018-10-25T12:00:00+02:00',
            'distance' => 1500,
            'time' => 1332,
            'location' => [
                'latitude' => -20.5,
                'longitude' => 124.2,
            ],
            'userId' => 3,
        ]);

        $this->assertEquals($expectedStatusCode, $response->getStatusCode());

        if (201 !== $expectedStatusCode) {
            return;
        }

        $this->assertResponseContains([
            '[id]' => 1,
            '[date]' => '2018-10-25T12:00:00+02:00',
            '[distance]' => 1500,
            '[time]' => 1332,
            '[location][latitude]' => -20.5,
            '[location][longitude]' => 124.2,
            '[userId]' => 3,
            '[temperature]' => 30.31,
            '[windSpeed]' => 4.07,
        ], $response);
        $this->assertEntityExists(Activity::class, 1);
    }

    /**
     * Data provider
     */
    public function createActivityTestDataProvider(): array
    {
        return [
            [null, 401],
            ['user-1', 201],
            ['manager', 403],
            ['admin', 201],
        ];
    }

    /**
     * @dataProvider dateValidationTestDataProvider
     */
    public function testDateValidation(array $data, bool $invalidFormatError = false)
    {
        $response = $this->sendRequest('POST', '/api/activity', 'user-1', $data);
        $content = $this->deserialize($response);

        $this->assertEquals(400, $response->getStatusCode());

        if ($invalidFormatError) {
            $this->assertStringStartsWith('Invalid datetime', $content['message'] ?? 'key missing');
        } else {
            $this->assertResponseContains(['[0][property_path]' => 'date'], $response);
        }
    }

    /**
     * Data provider
     */
    public function dateValidationTestDataProvider(): array
    {
        return [
            [[]], // Missing
            [['date' => ''], true], // Invalid format
            [['date' => 'invalid'], true], // Invalid format
        ];
    }

    /**
     * @dataProvider distanceValidationTestDataProvider
     */
    public function testDistanceValidation(array $data)
    {
        $response = $this->sendRequest('POST', '/api/activity', 'user-1', $data);

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertResponseContains(['[1][property_path]' => 'distance'], $response);
    }

    /**
     * Data provider
     */
    public function distanceValidationTestDataProvider(): array
    {
        return [
            [[]], // Missing
            [['distance' => '']], // Invalid format
            [['distance' => 'invalid']], // Invalid format
            [['distance' => -1]], // Invalid value
            [['distance' => 0]], // Invalid value
        ];
    }

    /**
     * @dataProvider timeValidationTestDataProvider
     */
    public function testTimeValidation(array $data)
    {
        $response = $this->sendRequest('POST', '/api/activity', 'user-1', $data);

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertResponseContains(['[2][property_path]' => 'time'], $response);
    }

    /**
     * Data provider
     */
    public function timeValidationTestDataProvider(): array
    {
        return [
            [[]], // Missing
            [['time' => '']], // Invalid format
            [['time' => 'invalid']], // Invalid format
            [['time' => -1]], // Invalid value
            [['time' => 0]], // Invalid value
        ];
    }

    /**
     * @dataProvider latitudeValidationTestDataProvider
     */
    public function testLatitudeValidation(array $data, bool $invalidFormatError = false, string $path = 'location.latitude')
    {
        $response = $this->sendRequest('POST', '/api/activity', 'user-1', $data);
        $content = $this->deserialize($response);

        $this->assertEquals(400, $response->getStatusCode());

        if ($invalidFormatError) {
            $this->assertStringStartsWith('Invalid data', $content['message'] ?? 'key missing');
        } else {
            $this->assertResponseContains(['[3][property_path]' => $path], $response);
        }
    }

    /**
     * Data provider
     */
    public function latitudeValidationTestDataProvider(): array
    {
        return [
            [[], false, 'location'], // Missing
            [['location' => null], false, 'location'], // Invalid format
            [['location' => ''], true], // Invalid format
            [['location' => 'isnvalid'], true], // Invalid format
            [['location' => -1], true], // Invalid value
            [['location' => 0], true], // Invalid value
            [['location' => []]], // Missing properties
            [['location' => ['latitude' => null]]], // Invalid value
            [['location' => ['latitude' => -90.001]]], // Invalid value
            [['location' => ['latitude' => 90.001]]], // Invalid value
        ];
    }

    /**
     * @dataProvider validLatitudeDataProvider
     */
    public function testValidLatitude($latitude, int $expectedValue = null)
    {
        $response = $this->sendRequest('POST', '/api/activity', 'user-1', [
            'date' => '2018-10-25T12:00:00+02:00',
            'distance' => 1500,
            'time' => 1332,
            'location' => [
                'latitude' => $latitude,
                'longitude' => 124.2,
            ],
        ]);

        $this->assertEquals(201, $response->getStatusCode());
        $this->assertResponseContains([
            '[id]' => 1,
            '[location][latitude]' => $expectedValue ?? $latitude
        ], $response);
    }

    /**
     * Data provider
     */
    public function validLatitudeDataProvider(): array
    {
        return [
            [-90],
            [90],
            [0],
            [-89.514],
            [89.514],
            ['asd', 0], // When casted to int = 0
        ];
    }

    /**
     * @dataProvider longitudeValidationTestDataProvider
     */
    public function testLongitudeValidation(array $data, bool $invalidFormatError = false, string $path = 'location.latitude')
    {
        $response = $this->sendRequest('POST', '/api/activity', 'user-1', $data);
        $content = $this->deserialize($response);

        $this->assertEquals(400, $response->getStatusCode());

        if ($invalidFormatError) {
            $this->assertStringStartsWith('Invalid data', $content['message'] ?? 'key missing');
        } else {
            $this->assertResponseContains(['[3][property_path]' => $path], $response);
        }
    }

    /**
     * Data provider
     */
    public function longitudeValidationTestDataProvider(): array
    {
        return [
            [[], false, 'location'], // Missing
            [['location' => null], false, 'location'], // Invalid format
            [['location' => ''], true], // Invalid format
            [['location' => 'isnvalid'], true], // Invalid format
            [['location' => -1], true], // Invalid value
            [['location' => 0], true], // Invalid value
            [['location' => []]], // Missing properties
            [['location' => ['longitude' => null]]], // Invalid value
            [['location' => ['longitude' => -180.001]]], // Invalid value
            [['location' => ['longitude' => 180.001]]], // Invalid value
        ];
    }

    /**
     * @dataProvider validLongitudeDataProvider
     */
    public function testValidLongitude($longitude, int $expectedValue = null)
    {
        $response = $this->sendRequest('POST', '/api/activity', 'user-1', [
            'date' => '2018-10-25T12:00:00+02:00',
            'distance' => 1500,
            'time' => 1332,
            'location' => [
                'latitude' => 15.222,
                'longitude' => $longitude,
            ],
        ]);

        $this->assertEquals(201, $response->getStatusCode());
        $this->assertResponseContains([
            '[id]' => 1,
            '[location][longitude]' => $expectedValue ?? $longitude
        ], $response);
    }

    /**
     * Data provider
     */
    public function validLongitudeDataProvider(): array
    {
        return [
            [-180],
            [180],
            [0],
            [-179.514],
            [179.514],
            ['asd', 0], // When casted to int = 0
        ];
    }

    /**
     * @dataProvider userValidationTestDataProvider
     */
    public function testUserValidation(array $data)
    {
        $response = $this->sendRequest('POST', '/api/activity', 'admin', $data);

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertResponseContains(['[4][property_path]' => 'userId'], $response);
    }

    /**
     * Data provider
     */
    public function userValidationTestDataProvider(): array
    {
        return [
            [[]], // Missing
            [['userId' => '']], // Invalid format
            [['userId' => 'invalid']], // Invalid format
            [['userId' => -1]], // Invalid value
            [['userId' => 0]], // Invalid value
            [['userId' => 1]], // Admin cannot have activities
            [['userId' => 2]], // Manager cannot have activities
            [['userId' => 5]], // User doesn't exist
        ];
    }

    /**
     * @dataProvider validUserIdDataProvider
     */
    public function testValidUserId(string $user, $userId, int $expectedUserId)
    {
        $response = $this->sendRequest('POST', '/api/activity', $user, [
            'date' => '2018-10-25T12:00:00+02:00',
            'distance' => 1500,
            'time' => 1332,
            'location' => [
                'latitude' => 15.222,
                'longitude' => 15.222,
            ],
            'userId' => $userId,
        ]);

        $this->assertEquals(201, $response->getStatusCode());
        $this->assertResponseContains(['[userId]' => $expectedUserId], $response);
    }

    /**
     * Data provider
     */
    public function validUserIdDataProvider(): array
    {
        return [
            ['user-1', 'asd', 3], // For customers userId property is not used
            ['user-1', null, 3], // For customers userId property is not used
            ['user-1', 1, 3], // For customers userId property is not used
            ['user-1', -20, 3], // For customers userId property is not used
            ['admin', 3, 3], // Valid user
            ['admin', 4, 4], // Valid user
        ];
    }
}
