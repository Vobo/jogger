<?php declare(strict_types=1);

namespace Tests\App\Controller\Activity;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Tests\App\Helper\DatabaseHelper;
use Tests\App\Helper\RequestTrait;

class ListActivitiesTest extends WebTestCase
{
    use DatabaseHelper;
    use RequestTrait;

    public static function setUpBeforeClass()
    {
        static::prepareDb([
            'setup.yaml',
            'activities.yaml',
            'more_activities.yaml',
        ]);
    }

    /**
     * @dataProvider listActivitiesDataProvider
     */
    public function testListActivities(?string $user, int $expectedStatusCode, array $responseContains = null)
    {
        $response = $this->sendRequest('GET', '/api/activity?limit=1', $user);

        $this->assertEquals($expectedStatusCode, $response->getStatusCode());

        if ($responseContains) {
            $this->assertResponseContains($responseContains, $response);
        }
    }

    /**
     * Data provider
     */
    public function listActivitiesDataProvider(): array
    {
        $adminList = [
            '[page]' => 1,
            '[pages]' => 45,
            '[limit]' => 1,
            '[activities][0][id]' => 1,
        ];

        $user1List = [
            '[page]' => 1,
            '[pages]' => 15,
            '[limit]' => 1,
            '[activities][0][id]' => 1,
        ];

        $user2List = [
            '[page]' => 1,
            '[pages]' => 30,
            '[limit]' => 1,
            '[activities][0][id]' => 12,
        ];

        return [
            [null, 401],
            ['user-1', 200, $user1List],
            ['user-2', 200, $user2List],
            ['manager', 403],
            ['admin', 200, $adminList],
        ];
    }

    /**
     * @dataProvider paginationTestDataProvider
     */
    public function testPagination($page, $limit, array $expectContains, int $expectedResults)
    {
        $url = sprintf('/api/activity?page=%s&limit=%s', $page, $limit);
        $response = $this->sendRequest('GET', $url, 'admin');

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertResponseContains($expectContains, $response);
        $data = $this->deserialize($response);
        $this->assertCount($expectedResults, $data['activities']);
    }

    /**
     * Data provider
     */
    public function paginationTestDataProvider(): array
    {
        $generator = function(int $page, int $pages, int $limit) {
            return [
                '[page]' => $page,
                '[pages]' => $pages,
                '[limit]' => $limit,
            ];
        };

        return [
            ['a', 'a', $generator(1, 5, 10), 10],
            [-10, -15, $generator(1, 5, 10), 10],
            [0, 0, $generator(1, 5, 10), 10],
            [1.5, 10.4, $generator(1, 5, 10), 10],
            ['2h', '6f', $generator(1, 5, 10), 10],
            [3, 11, $generator(3, 5, 11), 11],
            [5, 10, $generator(5, 5, 10), 5],
        ];
    }

    /**
     * @dataProvider filtersDataProvider
     */
    public function testFilters(string $user, string $filter, array $responseContains)
    {
        $response = $this->sendRequest('GET', '/api/activity?limit=1&filter='.$filter, $user);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertResponseContains($responseContains, $response);
    }

    /**
     * Data provider
     */
    public function filtersDataProvider(): array
    {
        return [
            ['admin', '', ['[pages]' => 45]], // Return all
            ['admin', '(user eq 1)', ['[pages]' => 0]],
            ['admin', '(user eq 4)', ['[pages]' => 30]],
            ['admin', '(user eq 3) or (user eq 4)', ['[pages]' => 45]],
            ['admin', '(id gt 5) and (id lt 10)', ['[pages]' => 4]],
            ['admin', '(date lt "2018-10-20")', ['[pages]' => 29]],
            ['admin', '(date lt "2018-10-20 11:00:00") and (date gt "2018-10-19 12:00:00")', ['[pages]' => 1]],
            ['admin', '(distance lte 401) or (distance gte 6400)', ['[pages]' => 2]],
            ['admin', '(time eq 155)', ['[pages]' => 16]],
            ['admin', '(latitude eq 123.321) or (latitude lt 50)', ['[pages]' => 17]],
            ['admin', '(longitude lt -140)', ['[pages]' => 18]],
            ['admin', '(temperature gt 62)', ['[pages]' => 4]],
            ['admin', '(temperature eq 12.2)', ['[pages]' => 10]],
            ['admin', '(windSpeed eq 36.4)', ['[pages]' => 0]],
            ['admin', '(windSpeed eq 36.5)', ['[pages]' => 30]],

            ['user-1', '', ['[pages]' => 15]], // Return all
            ['user-1', '(id gt 40)', ['[pages]' => 4]],
            ['user-1', '(distance lt 3000)', ['[pages]' => 6]],
            ['user-1', '(time lte 4000)', ['[pages]' => 12]],
        ];
    }
}
