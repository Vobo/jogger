<?php declare(strict_types=1);

namespace Tests\App\Controller\Activity;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Tests\App\Helper\DatabaseHelper;
use Tests\App\Helper\RequestTrait;

class GetActivityTest extends WebTestCase
{
    use DatabaseHelper;
    use RequestTrait;

    public static function setUpBeforeClass()
    {
        static::prepareDb([
            'setup.yaml',
            'activities.yaml',
        ]);
    }

    /**
     * @dataProvider getActivityDataProvider
     */
    public function testUserGetsActivity(?string $user, int $expectedStatusCode, array $expectedContent = null)
    {
        $response = $this->sendRequest('GET', '/api/activity/1', $user);
        $this->assertEquals($expectedStatusCode, $response->getStatusCode());

        if ($expectedContent) {
            $this->assertResponseContains($expectedContent, $response);
        }
    }

    /**
     * Data provider
     */
    public function getActivityDataProvider(): array
    {
        $content = [
            '[id]' => 1,
            '[date]' => '2018-10-20T12:00:00+02:00',
            '[distance]' => 401,
            '[time]' => 301,
            '[location][latitude]' => 15,
            '[location][longitude]' => -10,
            '[userId]' => 3,
            '[temperature]' => 5,
            '[windSpeed]' => 7,
        ];

        return [
            ['user-1', 200, $content], // Can see your own activity
            ['user-2', 404], // But others can't
            ['manager', 403], // Managers can't access activities at all
            ['admin', 200, $content], // But admins can
            [null, 401], // Unauthorized user is denied
        ];
    }
}
