<?php declare(strict_types=1);

namespace Tests\App\Controller\User;

use App\Entity\Activity;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Tests\App\Helper\DatabaseHelper;
use Tests\App\Helper\RequestTrait;

class UpdateActivityTest extends WebTestCase
{
    use DatabaseHelper;
    use RequestTrait;

    public function setUp()
    {
        static::prepareDb([
            'setup.yaml',
            'activities.yaml',
        ]);
    }

    /**
     * @dataProvider whoCanChangeWhoDataProvider
     */
    public function testWhoCanChangeWho(string $user, int $activityId, int $expectedStatusCode)
    {
        $response = $this->sendRequest('PUT', '/api/activity/'.$activityId, $user, [
            'date' => '2018-10-28T16:30:20+02:00',
            'distance' => 7,
            'time' => 8,
            'location' => [
                'latitude' => 10,
                'longitude' => 10,
            ],
        ]);

        $this->assertEquals($expectedStatusCode, $response->getStatusCode());
    }

    /**
     * Data provider
     */
    public function whoCanChangeWhoDataProvider(): array
    {
        return [
            ['user-1', 1, 200],
            ['user-1', 2, 200],
            ['user-1', 5, 404],

            ['user-2', 1, 404],
            ['user-2', 2, 404],
            ['user-2', 5, 200],

            ['manager', 1, 403],
            ['manager', 2, 403],
            ['manager', 5, 403],

            ['admin', 1, 200],
            ['admin', 2, 200],
            ['admin', 5, 200],
        ];
    }

    public function testUpdateActivity()
    {
        $response = $this->sendRequest('PUT', '/api/activity/2', 'user-1', [
            'date' => '2018-10-28T16:30:20+02:00',
            'distance' => 7,
            'time' => 8,
            'location' => [
                'latitude' => 10.5,
                'longitude' => -10.5,
            ],
            'temperature' => 10,
            'windSpeed' => 10,
        ]);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertResponseContains([
            '[date]' => '2018-10-28T16:30:20+02:00',
            '[distance]' => 7,
            '[time]' => 8,
            '[location][latitude]' => 10.5,
            '[location][longitude]' => -10.5,
            '[userId]' => 3,
            '[temperature]' => 30.31,
            '[windSpeed]' => 4.07,

        ], $response);

        /** @var Activity $entity */
        $entity = $this->fetchEntity(Activity::class, 2);

        $this->assertEquals(7, $entity->getDistance());
        $this->assertEquals(8, $entity->getTime());
        $this->assertEquals(10.5, $entity->getLocation()->getLatitude());
        $this->assertEquals(-10.5, $entity->getLocation()->getLongitude());
        $this->assertEquals(30.31, $entity->getTemperature());
        $this->assertEquals(4.07, $entity->getWindSpeed());
    }

    /**
     * @dataProvider dateValidationTestDataProvider
     */
    public function testDateValidation(array $data, bool $invalidFormatError = false)
    {
        $response = $this->sendRequest('PUT', '/api/activity/1', 'user-1', $data);
        $content = $this->deserialize($response);

        $this->assertEquals(400, $response->getStatusCode());

        if ($invalidFormatError) {
            $this->assertStringStartsWith('Invalid datetime', $content['message'] ?? 'key missing');
        } else {
            $this->assertResponseContains(['[0][property_path]' => 'date'], $response);
        }
    }

    /**
     * Data provider
     */
    public function dateValidationTestDataProvider(): array
    {
        return [
            [[]], // Missing
            [['date' => ''], true], // Invalid format
            [['date' => 'invalid'], true], // Invalid format
        ];
    }

    /**
     * @dataProvider distanceValidationTestDataProvider
     */
    public function testDistanceValidation(array $data)
    {
        $response = $this->sendRequest('PUT', '/api/activity/1', 'user-1', $data);

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertResponseContains(['[1][property_path]' => 'distance'], $response);
    }

    /**
     * Data provider
     */
    public function distanceValidationTestDataProvider(): array
    {
        return [
            [[]], // Missing
            [['distance' => '']], // Invalid format
            [['distance' => 'invalid']], // Invalid format
            [['distance' => -1]], // Invalid value
            [['distance' => 0]], // Invalid value
        ];
    }

    /**
     * @dataProvider timeValidationTestDataProvider
     */
    public function testTimeValidation(array $data)
    {
        $response = $this->sendRequest('PUT', '/api/activity/1', 'user-1', $data);

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertResponseContains(['[2][property_path]' => 'time'], $response);
    }

    /**
     * Data provider
     */
    public function timeValidationTestDataProvider(): array
    {
        return [
            [[]], // Missing
            [['time' => '']], // Invalid format
            [['time' => 'invalid']], // Invalid format
            [['time' => -1]], // Invalid value
            [['time' => 0]], // Invalid value
        ];
    }

    /**
     * @dataProvider latitudeValidationTestDataProvider
     */
    public function testLatitudeValidation(array $data, bool $invalidFormatError = false, string $path = 'location.latitude')
    {
        $response = $this->sendRequest('PUT', '/api/activity/1', 'user-1', $data);
        $content = $this->deserialize($response);

        $this->assertEquals(400, $response->getStatusCode());

        if ($invalidFormatError) {
            $this->assertStringStartsWith('Invalid data', $content['message'] ?? 'key missing');
        } else {
            $this->assertResponseContains(['[3][property_path]' => $path], $response);
        }
    }

    /**
     * Data provider
     */
    public function latitudeValidationTestDataProvider(): array
    {
        return [
            [[], false, 'location'], // Missing
            [['location' => null], false, 'location'], // Invalid format
            [['location' => ''], true], // Invalid format
            [['location' => 'isnvalid'], true], // Invalid format
            [['location' => -1], true], // Invalid value
            [['location' => 0], true], // Invalid value
            [['location' => []]], // Missing properties
            [['location' => ['latitude' => null]]], // Invalid value
            [['location' => ['latitude' => -90.001]]], // Invalid value
            [['location' => ['latitude' => 90.001]]], // Invalid value
        ];
    }

    /**
     * @dataProvider validLatitudeDataProvider
     */
    public function testValidLatitude($latitude, int $expectedValue = null)
    {
        $response = $this->sendRequest('PUT', '/api/activity/1', 'user-1', [
            'date' => '2018-10-25T12:00:00+02:00',
            'distance' => 1500,
            'time' => 1332,
            'location' => [
                'latitude' => $latitude,
                'longitude' => 124.2,
            ],
        ]);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertResponseContains([
            '[id]' => 1,
            '[location][latitude]' => $expectedValue ?? $latitude
        ], $response);
    }

    /**
     * Data provider
     */
    public function validLatitudeDataProvider(): array
    {
        return [
            [-90],
            [90],
            [0],
            [-89.514],
            [89.514],
            ['asd', 0], // When casted to int = 0
        ];
    }

    /**
     * @dataProvider longitudeValidationTestDataProvider
     */
    public function testLongitudeValidation(array $data, bool $invalidFormatError = false, string $path = 'location.latitude')
    {
        $response = $this->sendRequest('PUT', '/api/activity/1', 'user-1', $data);
        $content = $this->deserialize($response);

        $this->assertEquals(400, $response->getStatusCode());

        if ($invalidFormatError) {
            $this->assertStringStartsWith('Invalid data', $content['message'] ?? 'key missing');
        } else {
            $this->assertResponseContains(['[3][property_path]' => $path], $response);
        }
    }

    /**
     * Data provider
     */
    public function longitudeValidationTestDataProvider(): array
    {
        return [
            [[], false, 'location'], // Missing
            [['location' => null], false, 'location'], // Invalid format
            [['location' => ''], true], // Invalid format
            [['location' => 'isnvalid'], true], // Invalid format
            [['location' => -1], true], // Invalid value
            [['location' => 0], true], // Invalid value
            [['location' => []]], // Missing properties
            [['location' => ['longitude' => null]]], // Invalid value
            [['location' => ['longitude' => -180.001]]], // Invalid value
            [['location' => ['longitude' => 180.001]]], // Invalid value
        ];
    }

    /**
     * @dataProvider validLongitudeDataProvider
     */
    public function testValidLongitude($longitude, int $expectedValue = null)
    {
        $response = $this->sendRequest('PUT', '/api/activity/1', 'user-1', [
            'date' => '2018-10-25T12:00:00+02:00',
            'distance' => 1500,
            'time' => 1332,
            'location' => [
                'latitude' => 15.222,
                'longitude' => $longitude,
            ],
        ]);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertResponseContains([
            '[id]' => 1,
            '[location][longitude]' => $expectedValue ?? $longitude
        ], $response);
    }

    /**
     * Data provider
     */
    public function validLongitudeDataProvider(): array
    {
        return [
            [-180],
            [180],
            [0],
            [-179.514],
            [179.514],
            ['asd', 0], // When casted to int = 0
        ];
    }
}
