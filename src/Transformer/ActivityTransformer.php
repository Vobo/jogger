<?php declare(strict_types=1);

namespace App\Transformer;

use App\DTO\Activity\ReadActivityCollectionDTO;
use App\DTO\Activity\ReadActivityDTO;
use App\DTO\Activity\WriteActivityDTO;
use App\Entity\Activity;
use App\Manager\Exception\NotFoundException;
use App\Manager\Query\Result;
use App\Manager\UserManager;

class ActivityTransformer
{
    /**
     * @var UserManager
     */
    private $userManager;

    public function __construct(
        UserManager $userManager
    ) {
        $this->userManager = $userManager;
    }

    /**
     * @param Activity $entity
     * @param ReadActivityDTO|null $model
     * @return ReadActivityDTO
     */
    public function entityToReadModel(Activity $entity, ReadActivityDTO $model = null): ReadActivityDTO
    {
        if (!$model) {
            $model = new ReadActivityDTO();
        }

        $userId = $entity->getUser()->getId();

        return $model
            ->setId($entity->getId())
            ->setDate($entity->getDate())
            ->setDistance($entity->getDistance())
            ->setLocation($entity->getLocation())
            ->setUserId($userId)
            ->setTemperature($entity->getTemperature())
            ->setTime($entity->getTime())
            ->setWindSpeed($entity->getWindSpeed());
    }

    /**
     * @param WriteActivityDTO $model
     * @param Activity|null $entity
     * @return Activity
     * @throws NotFoundException
     */
    public function modelToEntity(WriteActivityDTO $model, Activity $entity = null): Activity
    {
        if (!$entity) {
            $entity = new Activity();
        }

        $user = $this->userManager->getUser($model->getUserId());

        return $entity
            ->setDate($model->getDate())
            ->setTime($model->getTime())
            ->setDistance($model->getDistance())
            ->setLocation($model->getLocation())
            ->setUser($user);
    }

    /**
     * @param Result $result
     * @param ReadActivityCollectionDTO|null $modelCollection
     * @return ReadActivityCollectionDTO
     */
    public function resultToReadModelCollection(
        Result $result,
        ReadActivityCollectionDTO $modelCollection = null
    ): ReadActivityCollectionDTO {
        if (!$modelCollection) {
            $modelCollection = new ReadActivityCollectionDTO();
        }

        $activities = array_map([$this, 'entityToReadModel'], $result->getResults());

        return $modelCollection
            ->setPage($result->getPage())
            ->setLimit($result->getSize())
            ->setPages($result->getPages())
            ->setActivities($activities);
    }
}
