<?php declare(strict_types=1);

namespace App\Transformer;

use App\DTO\User\ReadUserCollectionDTO;
use App\DTO\User\ReadUserDTO;
use App\DTO\User\WriteUserDTO;
use App\Entity\User;
use App\Manager\Query\Result;

class UserTransformer
{
    /**
     * @param User $entity
     * @param ReadUserDTO|null $model
     * @return ReadUserDTO
     */
    public function entityToModel(User $entity, ReadUserDTO $model = null): ReadUserDTO
    {
        if (!$model) {
            $model = new ReadUserDTO();
        }

        return $model
            ->setId($entity->getId())
            ->setUsername($entity->getUsername())
            ->setEmail($entity->getEmail())
            ->setRole($entity->getMainRole());
    }

    /**
     * @param WriteUserDTO $model
     * @param User|null $entity
     * @return User
     */
    public function modelToEntity(WriteUserDTO $model, User $entity = null): User
    {
        if (!$entity) {
            $entity = new User();
        }

        if ($model->getRole()) {
            $entity->setRoles([$model->getRole()]);
        }

        if ($model->getPassword()) {
            $entity->setPlainPassword($model->getPassword());
        }

        return $entity
            ->setUsername($model->getUsername())
            ->setEmail($model->getEmail());
    }

    /**
     * @param Result $result
     * @param ReadUserCollectionDTO|null $modelCollection
     * @return ReadUserCollectionDTO
     */
    public function resultToModelCollection(
        Result $result,
        ReadUserCollectionDTO $modelCollection = null
    ): ReadUserCollectionDTO {
        if (!$modelCollection) {
            $modelCollection = new ReadUserCollectionDTO();
        }

        $users = array_map([$this, 'entityToModel'], $result->getResults());

        return $modelCollection
            ->setPage($result->getPage())
            ->setLimit($result->getSize())
            ->setPages($result->getPages())
            ->setUsers($users);
    }
}
