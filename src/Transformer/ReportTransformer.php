<?php declare(strict_types=1);

namespace App\Transformer;

use App\DTO\Report\ReadReportCollectionDTO;
use App\Manager\Query\Result;

class ReportTransformer
{
    /**
     * @param Result $result
     * @param ReadReportCollectionDTO|null $modelCollection
     * @return ReadReportCollectionDTO
     */
    public function resultToModelCollection(
        Result $result,
        ReadReportCollectionDTO $modelCollection = null
    ): ReadReportCollectionDTO {
        if (!$modelCollection) {
            $modelCollection = new ReadReportCollectionDTO();
        }

        return $modelCollection
            ->setPage($result->getPage())
            ->setLimit($result->getSize())
            ->setPages($result->getPages())
            ->setRecords($result->getResults());
    }
}
