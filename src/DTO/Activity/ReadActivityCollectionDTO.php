<?php declare(strict_types=1);

namespace App\DTO\Activity;

class ReadActivityCollectionDTO
{
    /**
     * @var int
     */
    private $page;

    /**
     * @var int
     */
    private $limit;

    /**
     * @var int
     */
    private $pages;

    /**
     * @var ReadActivityDTO[]
     */
    private $activities;

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     * @return $this
     */
    public function setPage(int $page): self
    {
        $this->page = $page;

        return $this;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     * @return $this
     */
    public function setLimit(int $limit): self
    {
        $this->limit = $limit;

        return $this;
    }

    /**
     * @return int
     */
    public function getPages(): int
    {
        return $this->pages;
    }

    /**
     * @param int $pages
     * @return $this
     */
    public function setPages(int $pages): self
    {
        $this->pages = $pages;

        return $this;
    }

    /**
     * @return ReadActivityDTO[]
     */
    public function getActivities(): array
    {
        return $this->activities;
    }

    /**
     * @param ReadActivityDTO[] $activities
     * @return $this
     */
    public function setActivities(array $activities): self
    {
        $this->activities = $activities;

        return $this;
    }
}
