<?php declare(strict_types=1);

namespace App\DTO\Activity;

use App\VO\Point;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;
use App\Service\Doctrine\Validator as CustomAssert;

class WriteActivityDTO
{
    /**
     * @var \DateTime
     *
     * @JMS\Type("DateTime")
     *
     * @Assert\NotNull()
     * @Assert\DateTime()
     */
    private $date;

    /**
     * Distance in meters
     *
     * @var int
     *
     * @JMS\Type("int")
     *
     * @Assert\NotBlank()
     * @Assert\GreaterThan(0)
     */
    private $distance;

    /**
     * Time in seconds
     *
     * @var int
     *
     * @JMS\Type("int")
     *
     * @Assert\NotBlank()
     * @Assert\GreaterThan(0)
     */
    private $time;

    /**
     * @var Point
     *
     * @JMS\Type("App\VO\Point")
     *
     * @Assert\NotNull()
     * @Assert\Valid()
     */
    private $location;

    /**
     * @var int|null
     *
     * @JMS\Type("int")
     *
     * @Assert\NotBlank(groups={"AdminCreate"})
     * @CustomAssert\User(groups={"AdminCreate"})
     */
    private $userId;

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     * @return $this
     */
    public function setDate(\DateTime $date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return int
     */
    public function getDistance(): int
    {
        return $this->distance;
    }

    /**
     * @param int $distance
     * @return $this
     */
    public function setDistance(int $distance): self
    {
        $this->distance = $distance;

        return $this;
    }

    /**
     * @return int
     */
    public function getTime(): int
    {
        return $this->time;
    }

    /**
     * @param int $time
     * @return $this
     */
    public function setTime(int $time): self
    {
        $this->time = $time;

        return $this;
    }

    /**
     * @return Point
     */
    public function getLocation(): Point
    {
        return $this->location;
    }

    /**
     * @param Point $location
     * @return $this
     */
    public function setLocation(Point $location): self
    {
        $this->location = $location;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->userId;
    }

    /**
     * @param int|null $userId
     * @return $this
     */
    public function setUserId(?int $userId): self
    {
        $this->userId = $userId;

        return $this;
    }
}
