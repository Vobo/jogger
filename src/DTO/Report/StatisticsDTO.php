<?php declare(strict_types=1);

namespace App\DTO\Report;

use JMS\Serializer\Annotation as JMS;

class StatisticsDTO
{
    /**
     * @var int
     *
     * @JMS\Type("int")
     */
    private $userId;

    /**
     * @var int
     *
     * @JMS\Type("int")
     */
    private $week;

    /**
     * @var int
     *
     * @JMS\Type("int")
     */
    private $year;

    /**
     * @var float
     *
     * @JMS\Type("float")
     */
    private $averageDistance;

    /**
     * @var float
     *
     * @JMS\Type("float")
     */
    private $averageTime;

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     * @return $this
     */
    public function setUserId(int $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * @return int
     */
    public function getWeek(): int
    {
        return $this->week;
    }

    /**
     * @param int $week
     * @return $this
     */
    public function setWeek(int $week): self
    {
        $this->week = $week;

        return $this;
    }

    /**
     * @return int
     */
    public function getYear(): int
    {
        return $this->year;
    }

    /**
     * @param int $year
     * @return $this
     */
    public function setYear(int $year): self
    {
        $this->year = $year;

        return $this;
    }

    /**
     * @return float
     */
    public function getAverageDistance(): float
    {
        return $this->averageDistance;
    }

    /**
     * @param float $averageDistance
     * @return $this
     */
    public function setAverageDistance(float $averageDistance): self
    {
        $this->averageDistance = $averageDistance;

        return $this;
    }

    /**
     * @return float
     */
    public function getAverageTime(): float
    {
        return $this->averageTime;
    }

    /**
     * @param float $averageTime
     * @return $this
     */
    public function setAverageTime(float $averageTime): self
    {
        $this->averageTime = $averageTime;

        return $this;
    }
}
