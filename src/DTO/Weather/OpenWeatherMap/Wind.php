<?php declare(strict_types=1);

namespace App\DTO\Weather\OpenWeatherMap;

use JMS\Serializer\Annotation as JMS;

class Wind
{
    /**
     * @var float
     *
     * @JMS\Type("float")
     */
    private $speed;

    /**
     * Wind direction
     *
     * @var float
     *
     * @JMS\Type("float")
     * @JMS\SerializedName("deg")
     */
    private $degrees;

    /**
     * @return float
     */
    public function getSpeed(): float
    {
        return $this->speed;
    }

    /**
     * @param float $speed
     * @return $this
     */
    public function setSpeed(float $speed): self
    {
        $this->speed = $speed;

        return $this;
    }

    /**
     * @return float
     */
    public function getDegrees(): float
    {
        return $this->degrees;
    }

    /**
     * @param float $degrees
     * @return $this
     */
    public function setDegrees(float $degrees): self
    {
        $this->degrees = $degrees;

        return $this;
    }
}
