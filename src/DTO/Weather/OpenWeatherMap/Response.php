<?php declare(strict_types=1);

namespace App\DTO\Weather\OpenWeatherMap;

use JMS\Serializer\Annotation as JMS;

class Response
{
    /**
     * @var Wind
     *
     * @JMS\Type("App\DTO\Weather\OpenWeatherMap\Wind")
     */
    private $wind;

    /**
     * @var Main
     *
     * @JMS\Type("App\DTO\Weather\OpenWeatherMap\Main")
     */
    private $main;

    /**
     * @return Wind
     */
    public function getWind(): Wind
    {
        return $this->wind;
    }

    /**
     * @param Wind $wind
     * @return $this
     */
    public function setWind(Wind $wind): self
    {
        $this->wind = $wind;

        return $this;
    }

    /**
     * @return Main
     */
    public function getMain(): Main
    {
        return $this->main;
    }

    /**
     * @param Main $main
     * @return $this
     */
    public function setMain(Main $main): self
    {
        $this->main = $main;

        return $this;
    }
}
