<?php declare(strict_types=1);

namespace App\DTO\Weather\OpenWeatherMap;

use JMS\Serializer\Annotation as JMS;

class Main
{
    /**
     * @var float
     *
     * @JMS\Type("float")
     * @JMS\SerializedName("temp")
     */
    private $temperature;

    /**
     * @var float
     *
     * @JMS\Type("float")
     */
    private $pressure;

    /**
     * Humidity %
     *
     * @var int
     *
     * @JMS\Type("int")
     */
    private $humidity;

    /**
     * @var float
     *
     * @JMS\Type("float")
     * @JMS\SerializedName("temp_min")
     */
    private $tempMin;

    /**
     * @var float
     *
     * @JMS\Type("float")
     * @JMS\SerializedName("temp_max")
     */
    private $tempMax;

    /**
     * @var float
     *
     * @JMS\Type("float")
     * @JMS\SerializedName("sea_level")
     */
    private $seaLevel;

    /**
     * @var float
     *
     * @JMS\Type("float")
     * @JMS\SerializedName("grnd_level")
     */
    private $groundLevel;

    /**
     * @return float
     */
    public function getTemperature(): float
    {
        return $this->temperature;
    }

    /**
     * @param float $temperature
     * @return $this
     */
    public function setTemperature(float $temperature): self
    {
        $this->temperature = $temperature;

        return $this;
    }

    /**
     * @return float
     */
    public function getPressure(): float
    {
        return $this->pressure;
    }

    /**
     * @param float $pressure
     * @return $this
     */
    public function setPressure(float $pressure): self
    {
        $this->pressure = $pressure;

        return $this;
    }

    /**
     * @return int
     */
    public function getHumidity(): int
    {
        return $this->humidity;
    }

    /**
     * @param int $humidity
     * @return $this
     */
    public function setHumidity(int $humidity): self
    {
        $this->humidity = $humidity;

        return $this;
    }

    /**
     * @return float
     */
    public function getTempMin(): float
    {
        return $this->tempMin;
    }

    /**
     * @param float $tempMin
     * @return $this
     */
    public function setTempMin(float $tempMin): self
    {
        $this->tempMin = $tempMin;

        return $this;
    }

    /**
     * @return float
     */
    public function getTempMax(): float
    {
        return $this->tempMax;
    }

    /**
     * @param float $tempMax
     * @return $this
     */
    public function setTempMax(float $tempMax): self
    {
        $this->tempMax = $tempMax;

        return $this;
    }

    /**
     * @return float
     */
    public function getSeaLevel(): float
    {
        return $this->seaLevel;
    }

    /**
     * @param float $seaLevel
     * @return $this
     */
    public function setSeaLevel(float $seaLevel): self
    {
        $this->seaLevel = $seaLevel;

        return $this;
    }

    /**
     * @return float
     */
    public function getGroundLevel(): float
    {
        return $this->groundLevel;
    }

    /**
     * @param float $groundLevel
     * @return $this
     */
    public function setGroundLevel(float $groundLevel): self
    {
        $this->groundLevel = $groundLevel;

        return $this;
    }
}
