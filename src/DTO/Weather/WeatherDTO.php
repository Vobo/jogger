<?php declare(strict_types=1);

namespace App\DTO\Weather;

class WeatherDTO
{
    /**
     * In Celsius
     *
     * @var float
     */
    private $temperature;

    /**
     * In meter/sec
     *
     * @var float
     */
    private $windSpeed;

    /**
     * @return float
     */
    public function getTemperature(): float
    {
        return $this->temperature;
    }

    /**
     * @param float $temperature
     * @return $this
     */
    public function setTemperature(float $temperature): self
    {
        $this->temperature = $temperature;

        return $this;
    }

    /**
     * @return float
     */
    public function getWindSpeed(): float
    {
        return $this->windSpeed;
    }

    /**
     * @param float $windSpeed
     * @return $this
     */
    public function setWindSpeed(float $windSpeed): self
    {
        $this->windSpeed = $windSpeed;

        return $this;
    }
}
