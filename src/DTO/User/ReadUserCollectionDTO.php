<?php declare(strict_types=1);

namespace App\DTO\User;

class ReadUserCollectionDTO
{
    /**
     * @var int
     */
    private $page;

    /**
     * @var int
     */
    private $limit;

    /**
     * @var int
     */
    private $pages;

    /**
     * @var ReadUserDTO[]
     */
    private $users;

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     * @return $this
     */
    public function setPage(int $page): self
    {
        $this->page = $page;

        return $this;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     * @return $this
     */
    public function setLimit(int $limit): self
    {
        $this->limit = $limit;

        return $this;
    }

    /**
     * @return int
     */
    public function getPages(): int
    {
        return $this->pages;
    }

    /**
     * @param int $pages
     * @return $this
     */
    public function setPages(int $pages): self
    {
        $this->pages = $pages;

        return $this;
    }

    /**
     * @return ReadUserDTO[]
     */
    public function getUsers(): array
    {
        return $this->users;
    }

    /**
     * @param ReadUserDTO[] $users
     * @return $this
     */
    public function setUsers(array $users): self
    {
        $this->users = $users;

        return $this;
    }
}
