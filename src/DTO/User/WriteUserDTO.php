<?php declare(strict_types=1);

namespace App\DTO\User;

use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;
use App\Service\Doctrine\Validator as CustomAssert;

/**
 *     fields={"username"},
 *     entityClass="App\Entity\User",
 *     em="default"
 *
 *     fields={"email"},
 *     entityClass="App\Entity\User",
 *     em="default"
 */


/**
 * @Assert\GroupSequence({"WriteUserDTO", "Strict"})
 */
class WriteUserDTO
{
    /**
     * @var string
     *
     * @JMS\Type("string")
     *
     * @Assert\NotBlank()
     * @Assert\Length(
     *     min=3,
     *     max=180
     * )
     * @CustomAssert\Unique(
     *     class="App\Entity\User",
     *     field="username",
     *     routeIdParam="userId"
     * )
     */
    private $username;

    /**
     * @var string
     *
     * @JMS\Type("string")
     *
     * @Assert\NotBlank()
     * @Assert\Length(
     *     min=3,
     *     max=180
     * )
     * @CustomAssert\Unique(
     *     class="App\Entity\User",
     *     field="email",
     *     routeIdParam="userId"
     * )
     * @Assert\Email(groups={"Strict"})
     */
    private $email;

    /**
     * @var string|null
     *
     * @JMS\Type("string")
     *
     * @Assert\NotBlank(groups={"Create"})
     * @Assert\Length(
     *     groups={"Create"},
     *     min=6,
     *     max=72
     * )
     */
    private $password;

    /**
     * @var string|null
     *
     * @JMS\Type("string")
     *
     * @CustomAssert\WhenRole(
     *     groups={"Create"},
     *     role="ROLE_MANAGER",
     *     constraints={
     *         @Assert\NotNull(),
     *         @Assert\Choice({"ROLE_CUSTOMER", "ROLE_MANAGER"})
     *     }
     * )
     * @CustomAssert\WhenRole(
     *     groups={"Create"},
     *     role="ROLE_ADMIN",
     *     constraints={
     *         @Assert\NotNull(),
     *         @Assert\Choice({"ROLE_CUSTOMER", "ROLE_MANAGER", "ROLE_ADMIN"})
     *     }
     * )
     */
    private $role;

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return $this
     */
    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return $this
     */
    public function setPassword(string $password = null): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getRole(): ?string
    {
        return $this->role;
    }

    /**
     * @param string|null $role
     * @return $this
     */
    public function setRole(?string $role): self
    {
        $this->role = $role;

        return $this;
    }
}
