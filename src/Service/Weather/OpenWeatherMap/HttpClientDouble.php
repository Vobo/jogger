<?php declare(strict_types=1);

namespace App\Service\Weather\OpenWeatherMap;

use GuzzleHttp\Psr7\Response;
use Http\Client\HttpClient;
use Psr\Http\Message\RequestInterface;

class HttpClientDouble implements HttpClient
{
    private const RESPONSE = <<<DOC
{
    "coord": {
        "lon": 124.2,
        "lat": -20.5
    },
    "weather": [{
        "id": 800,
        "main": "Clear",
        "description": "clear sky",
        "icon": "01n"
    }],
    "base": "stations",
    "main": {
        "temp": 30.31,
        "pressure": 988.86,
        "humidity": 16,
        "temp_min": 19.41,
        "temp_max": 32.76,
        "sea_level": 1021.59,
        "grnd_level": 988.86
    },
    "wind": {
        "speed": 4.07,
        "deg": 225.001
    },
    "clouds": {
        "all": 0
    },
    "dt": 1540211987,
    "sys": {
        "message": 0.0131,
        "country": "AU",
        "sunrise": 1540156032,
        "sunset": 1540201699
    },
    "id": 7839505,
    "name": "East Pilbara",
    "cod": 200
}
DOC;

    /**
     * @inheritdoc
     */
    public function sendRequest(RequestInterface $request)
    {
        return new Response(200,[], self::RESPONSE);
    }
}
