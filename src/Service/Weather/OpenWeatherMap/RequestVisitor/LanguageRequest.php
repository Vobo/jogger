<?php declare(strict_types=1);

namespace App\Service\Weather\OpenWeatherMap\RequestVisitor;

use App\Service\Weather\OpenWeatherMap\Request\RequestBuilder;

class LanguageRequest implements RequestVisitorInterface
{
    public const LANGUAGE_Arabic = 'ar';
    public const LANGUAGE_Bulgarian = 'bg';
    public const LANGUAGE_Catalan = 'ca';
    public const LANGUAGE_Czech = 'cz';
    public const LANGUAGE_German = 'de';
    public const LANGUAGE_Greek = 'el';
    public const LANGUAGE_English = 'en';
    public const LANGUAGE_Persian = 'fa';
    public const LANGUAGE_Finnish = 'fi';
    public const LANGUAGE_French = 'fr';
    public const LANGUAGE_Galician = 'gl';
    public const LANGUAGE_Croatian = 'hr';
    public const LANGUAGE_Hungarian = 'hu';
    public const LANGUAGE_Italian = 'it';
    public const LANGUAGE_Japanese = 'ja';
    public const LANGUAGE_Korean = 'kr';
    public const LANGUAGE_Latvian = 'la';
    public const LANGUAGE_Lithuanian = 'lt';
    public const LANGUAGE_Macedonian = 'mk';
    public const LANGUAGE_Dutch = 'nl';
    public const LANGUAGE_Polish = 'pl';
    public const LANGUAGE_Portuguese = 'pt';
    public const LANGUAGE_Romanian = 'ro';
    public const LANGUAGE_Russian = 'ru';
    public const LANGUAGE_Swedish = 'se';
    public const LANGUAGE_Slovak = 'sk';
    public const LANGUAGE_Slovenian = 'sl';
    public const LANGUAGE_Spanish = 'es';
    public const LANGUAGE_Turkish = 'tr';
    public const LANGUAGE_Ukrainian = 'ua';
    public const LANGUAGE_Vietnamese = 'vi';
    public const LANGUAGE_Chinese_Simplified = 'zh_cn';
    public const LANGUAGE_Chinese_Traditional = 'zh_tw';

    private const PARAM_KEY = 'lang';
    
    /**
     * @var string
     */
    private $language;

    public function __construct(string $language)
    {
        $reflection = new \ReflectionClass($this);
        $constants = $reflection->getConstants();

        if (!in_array($language, $constants)) {
            throw new \InvalidArgumentException('Invalid language');
        }

        $this->language = $language;
    }

    /**
     * @inheritdoc
     */
    public function visit(RequestBuilder $builder): void
    {
        $builder->setQueryFragment(self::PARAM_KEY, $this->language);
    }
}
