<?php declare(strict_types=1);

namespace App\Service\Weather\OpenWeatherMap\RequestVisitor;

use App\Service\Weather\OpenWeatherMap\Request\RequestBuilder;
use App\VO\Point;

class RequestByCoordinates implements RequestVisitorInterface
{
    private const PARAM_LATITUDE = 'lat';
    private const PARAM_LONGITUDE = 'lon';

    /**
     * @var Point
     */
    private $point;

    public function __construct(Point $point)
    {
        $this->point = $point;
    }

    /**
     * @inheritdoc
     */
    public function visit(RequestBuilder $builder): void
    {
        $builder
            ->setQueryFragment(self::PARAM_LATITUDE, (string)$this->point->getLatitude())
            ->setQueryFragment(self::PARAM_LONGITUDE, (string)$this->point->getLongitude());
    }
}
