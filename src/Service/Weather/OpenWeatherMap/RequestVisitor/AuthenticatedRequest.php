<?php declare(strict_types=1);

namespace App\Service\Weather\OpenWeatherMap\RequestVisitor;

use App\Service\Weather\OpenWeatherMap\Request\RequestBuilder;

class AuthenticatedRequest implements RequestVisitorInterface
{
    private const PARAM_KEY = 'appid';

    /**
     * @var string
     */
    private $apiKey;

    public function __construct(string $apiKey)
    {
        $this->apiKey = $apiKey;
    }

    /**
     * @inheritdoc
     */
    public function visit(RequestBuilder $builder): void
    {
        $builder->setQueryFragment(self::PARAM_KEY, $this->apiKey);
    }
}
