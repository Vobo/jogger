<?php declare(strict_types=1);

namespace App\Service\Weather\OpenWeatherMap\RequestVisitor;

use App\Service\Weather\OpenWeatherMap\Request\RequestBuilder;

interface RequestVisitorInterface
{
    /**
     * @param RequestBuilder $builder
     */
    public function visit(RequestBuilder $builder): void;
}
