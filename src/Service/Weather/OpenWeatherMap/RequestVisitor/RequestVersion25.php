<?php declare(strict_types=1);

namespace App\Service\Weather\OpenWeatherMap\RequestVisitor;

use App\Service\Weather\OpenWeatherMap\Request\RequestBuilder;

class RequestVersion25 implements RequestVisitorInterface
{
    private const METHOD = 'GET';
    private const PROTOCOL = 'HTTP';
    private const HOST = 'api.openweathermap.org';
    private const URI  ='/data/2.5/weather';

    /**
     * @inheritdoc
     */
    public function visit(RequestBuilder $builder): void
    {
        $builder
            ->setMethod(self::METHOD)
            ->setProtocol(self::PROTOCOL)
            ->setHost(self::HOST)
            ->setUri(self::URI);
    }
}
