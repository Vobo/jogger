<?php declare(strict_types=1);

namespace App\Service\Weather\OpenWeatherMap\RequestVisitor;

use App\Service\Weather\OpenWeatherMap\Request\RequestBuilder;

class UnitsRequest implements RequestVisitorInterface
{
    public const UNITS_IMPERIAL = 'imperial';
    public const UNITS_METRIC = 'metric';

    private const PARAM_UNITS = 'units';

    /**
     * @var string
     */
    private $units;

    public function __construct(string $units)
    {
        if (!in_array($units, [self::UNITS_IMPERIAL, self::UNITS_METRIC])) {
            throw new \InvalidArgumentException('Invalid units');
        }

        $this->units = $units;
    }

    /**
     * @inheritdoc
     */
    public function visit(RequestBuilder $builder): void
    {
        $builder->setQueryFragment(self::PARAM_UNITS, $this->units);
    }
}
