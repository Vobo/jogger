<?php declare(strict_types=1);

namespace App\Service\Weather\OpenWeatherMap;

use App\Service\Weather\OpenWeatherMap\Exception\ClientException;
use App\Service\Weather\OpenWeatherMap\Exception\UnauthorizedException;
use App\Service\Weather\OpenWeatherMap\Request\RequestFactory;
use App\Service\Weather\OpenWeatherMap\RequestVisitor\RequestByCoordinates;
use App\DTO\Weather\OpenWeatherMap\Response;
use App\VO\Point;
use Http\Client\HttpClient;
use JMS\Serializer\SerializerInterface;
use Psr\Http\Message\ResponseInterface;

class Client
{
    /**
     * @var RequestFactory
     */
    private $requestFactory;

    /**
     * @var HttpClient
     */
    private $httpClient;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    public function __construct(
        RequestFactory $requestFactory,
        HttpClient $httpClient,
        SerializerInterface $serializer
    ) {
        $this->requestFactory = $requestFactory;
        $this->httpClient = $httpClient;
        $this->serializer = $serializer;
    }

    /**
     * @param Point $point
     * @return Response
     * @throws ClientException
     */
    public function getWeather(Point $point): Response
    {
        $request = $this->requestFactory->create([
            new RequestByCoordinates($point),
        ]);

        $response = $this->httpClient->sendRequest($request);

        if (200 !== $response->getStatusCode()) {
            throw $this->handleError($response);
        }

        return $this->serializer->deserialize(
            $response->getBody()->getContents(),
                Response::class,
                'json'
        );
    }

    /**
     * @param ResponseInterface $response
     * @return ClientException
     */
    private function handleError(ResponseInterface $response): ClientException
    {
        switch ($response->getStatusCode()) {
            case 401:
                return new UnauthorizedException($response);

            default:
                return new ClientException($response);
        }
    }
}
