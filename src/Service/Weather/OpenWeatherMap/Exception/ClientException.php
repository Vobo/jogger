<?php declare(strict_types=1);

namespace App\Service\Weather\OpenWeatherMap\Exception;

use Psr\Http\Message\ResponseInterface;
use Throwable;

class ClientException extends \Exception
{
    public function __construct(ResponseInterface $response, Throwable $previous = null)
    {
        parent::__construct(
            'OpenWeatherMap error: '.$response->getBody()->getContents(),
            $response->getStatusCode(),
            $previous
        );
    }
}
