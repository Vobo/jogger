<?php declare(strict_types=1);

namespace App\Service\Weather\OpenWeatherMap\Exception;

class UnauthorizedException extends ClientException
{

}
