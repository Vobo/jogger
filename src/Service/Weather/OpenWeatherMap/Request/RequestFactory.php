<?php declare(strict_types=1);

namespace App\Service\Weather\OpenWeatherMap\Request;

use App\Service\Weather\OpenWeatherMap\RequestVisitor\RequestVisitorInterface;
use Psr\Http\Message\RequestInterface;

class RequestFactory
{
    /**
     * @var RequestVisitorInterface[]|array
     */
    private $defaultVisitors;

    /**
     * @param RequestVisitorInterface[] $defaultVisitors
     */
    public function __construct(array $defaultVisitors)
    {
        $this->defaultVisitors = $defaultVisitors;
    }

    /**
     * @param RequestVisitorInterface[] $customVisitors
     * @return RequestInterface
     */
    public function create(array $customVisitors): RequestInterface
    {
        $builder = new RequestBuilder();

        /** @var RequestVisitorInterface $visitor */
        foreach (array_merge($this->defaultVisitors, $customVisitors) as $visitor) {
            $visitor->visit($builder);
        }

        return $builder->build();
    }
}
