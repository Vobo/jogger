<?php declare(strict_types=1);

namespace App\Service\Weather\OpenWeatherMap\Request;

use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\RequestInterface;

class RequestBuilder
{
    /**
     * @var string
     */
    private $method;

    /**
     * @var string
     */
    private $protocol;

    /**
     * @var string
     */
    private $host;

    /**
     * @var string|null
     */
    private $uri;

    /**
     * @var string[]
     */
    private $queryFragments = [];

    /**
     * @var string[]
     */
    private $headers = [];

    /**
     * @param string $method
     * @return RequestBuilder
     */
    public function setMethod(string $method): self
    {
        $this->method = $method;

        return $this;
    }

    /**
     * @param string $protocol
     * @return RequestBuilder
     */
    public function setProtocol(string $protocol): self
    {
        $this->protocol = $protocol;

        return $this;
    }

    /**
     * @param string $host
     * @return RequestBuilder
     */
    public function setHost(string $host): self
    {
        $this->host = $host;

        return $this;
    }

    /**
     * @param null|string $uri
     * @return RequestBuilder
     */
    public function setUri(?string $uri): self
    {
        $this->uri = $uri;

        return $this;
    }

    /**
     * @param string $key
     * @param string $value
     * @return RequestBuilder
     */
    public function setQueryFragment(string $key, string $value): self
    {
        $this->queryFragments[$key] = $value;

        return $this;
    }

    /**
     * @param string $header
     * @param string $value
     * @return RequestBuilder
     */
    public function setHeader(string $header, string $value): self
    {
        $this->headers = $header;

        return $this;
    }

    /**
     * @return RequestInterface
     * @throws \Exception
     */
    public function build(): RequestInterface
    {
        if (!$this->method) {
            throw new \Exception('Missing method');
        }

        if (!$this->host) {
            throw new \Exception('Missing host');
        }

        return new Request(
            $this->method,
            $this->buildUrl(),
            $this->headers
        );
    }

    /**
     * @return string
     */
    private function buildUrl(): string
    {
        return sprintf(
            '%s://%s%s?%s',
            $this->protocol,
            $this->host,
            $this->uri,
            http_build_query($this->queryFragments)
        );
    }
}
