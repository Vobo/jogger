<?php declare(strict_types=1);

namespace App\Service\Weather;

use App\DTO\Weather\WeatherDTO;
use App\Service\Weather\OpenWeatherMap\Client;
use App\VO\Point;

class OpenWeatherMapClientProxy implements ClientInterface
{
    /**
     * @var Client
     */
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @inheritdoc
     */
    public function getByCoordinates(Point $point): WeatherDTO
    {
        $response = $this->client->getWeather($point);

        return (new WeatherDTO())
            ->setWindSpeed($response->getWind()->getSpeed())
            ->setTemperature($response->getMain()->getTemperature());
    }

    /**
     * @inheritdoc
     */
    public function getHistoricByCoordinates(\DateTime $date, Point $point): WeatherDTO
    {
        // Not implemented since as no free API is available
        return $this->getByCoordinates($point);
    }
}
