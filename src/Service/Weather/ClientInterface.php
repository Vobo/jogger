<?php declare(strict_types=1);

namespace App\Service\Weather;

use App\DTO\Weather\WeatherDTO;
use App\VO\Point;

interface ClientInterface
{
    /**
     * @param Point $point
     * @return WeatherDTO
     */
    public function getByCoordinates(Point $point): WeatherDTO;

    /**
     * @param \DateTime $date
     * @param Point $point
     * @return WeatherDTO
     */
    public function getHistoricByCoordinates(\DateTime $date, Point $point): WeatherDTO;
}
