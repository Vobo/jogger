<?php declare(strict_types=1);

namespace App\Service\Voter;

use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class OwnerVoter extends Voter
{
    public const ATTRIBUTE = 'owner';

    /**
     * @var OwnerResolver
     */
    private $ownerResolver;

    public function __construct(
        OwnerResolver $ownerResolver
    ) {
        $this->ownerResolver = $ownerResolver;
    }

    /**
     * @inheritdoc
     */
    protected function supports($attribute, $subject)
    {
        if (self::ATTRIBUTE !== $attribute) {
            return false;
        }

        foreach ($this->ownerResolver->getSupportedClasses() as $supportedClass) {
            if ($subject instanceof $supportedClass) {
                return true;
            }
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false; // User not authenticated
        }

        return $this->ownerResolver->isOwner($user, $subject);
    }
}
