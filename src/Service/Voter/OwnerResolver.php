<?php declare(strict_types=1);

namespace App\Service\Voter;

use App\Entity\User;
use App\Service\Voter\OwnerResolver\OwnerResolverInterface;

class OwnerResolver
{
    /**
     * Indexed by class name
     *
     * @var OwnerResolverInterface[]
     */
    protected $ownerResolvers = [];

    /**
     * @param OwnerResolverInterface $ownerResolver
     */
    public function registerOwnerResolver(OwnerResolverInterface $ownerResolver): void
    {
        $this->ownerResolvers[$ownerResolver->getClass()] = $ownerResolver;
    }

    /**
     * @return array
     */
    public function getSupportedClasses(): array
    {
        return array_keys($this->ownerResolvers);
    }

    /**
     * @param User $user
     * @param object $object
     * @return bool
     */
    public function isOwner(User $user, object $object): bool
    {
        $resolver = $this->getResolver($object);

        return $resolver->isOwner($user, $object);
    }

    /**
     * @param object $object
     * @return OwnerResolverInterface
     * @throws \InvalidArgumentException
     */
    private function getResolver(object $object): OwnerResolverInterface
    {
        foreach ($this->ownerResolvers as $resolver) {
            $class = $resolver->getClass();

            if ($object instanceof $class) {
                return $resolver;
            }
        }

        throw new \InvalidArgumentException('Object not supported: '.get_class($object));
    }
}
