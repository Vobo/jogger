<?php declare(strict_types=1);

namespace App\Service\Voter\OwnerResolver;

use App\Entity\Activity;
use App\Entity\User;
use Symfony\Component\Security\Core\Security;

class ActivityOwnerResolver implements OwnerResolverInterface
{
    /**
     * @var Security
     */
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * @inheritdoc
     */
    public function getClass(): string
    {
        return Activity::class;
    }

    /**
     * @inheritdoc
     * @param Activity $object
     */
    public function isOwner(User $user, object $object): bool
    {
        if ($this->security->isGranted(User::ROLE_ADMIN)) {
            return true;
        }

        return $object->getUser() === $user;
    }
}
