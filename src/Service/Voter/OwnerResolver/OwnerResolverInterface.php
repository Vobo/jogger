<?php declare(strict_types=1);

namespace App\Service\Voter\OwnerResolver;

use App\Entity\User;

interface OwnerResolverInterface
{
    /**
     * @return string Class supported by OwnerResolver
     */
    public function getClass(): string;

    /**
     * @param User $user
     * @param object $object Object of class {$this->>getClass()}
     * @return bool
     */
    public function isOwner(User $user, object $object): bool;
}
