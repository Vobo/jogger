<?php declare(strict_types=1);

namespace App\Service\Voter\OwnerResolver;

use App\Entity\User;

class UserOwnerResolver implements OwnerResolverInterface
{
    private const ROLE_DEPENDENCIES = [
        User::ROLE_ADMIN => [
            User::ROLE_ADMIN,
            User::ROLE_MANAGER,
            User::ROLE_CUSTOMER,
        ],
        User::ROLE_MANAGER => [
            User::ROLE_CUSTOMER,
        ],
        User::ROLE_CUSTOMER => [],
    ];

    /**
     * @inheritdoc
     */
    public function getClass(): string
    {
        return User::class;
    }

    /**
     * @inheritdoc
     * @param User $object
     */
    public function isOwner(User $user, object $object): bool
    {
        $ownerRole = $user->getMainRole();

        return in_array($object->getMainRole(), self::ROLE_DEPENDENCIES[$ownerRole])
            || $user === $object;
    }
}
