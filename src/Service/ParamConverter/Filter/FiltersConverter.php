<?php declare(strict_types=1);

namespace App\Service\ParamConverter\Filter;

use App\Entity\User;
use App\Manager\Query\Filter;
use App\Service\ParamConverter\Filter\Config\ColumnRegistry;
use App\Service\ParamConverter\Filter\Config\ConfigRegisters;
use App\Service\ParamConverter\Filter\Parser\Parser;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;

class FiltersConverter implements ParamConverterInterface
{
    public const CONVERTER = 'filters';

    public const OPTION_FILTERS_CUSTOMER = 'customer';
    public const OPTION_FILTERS_MANAGER = 'manager';
    public const OPTION_FILTERS_ADMIN = 'admin';

    private const OPTIONS_FILTERS = [
        User::ROLE_CUSTOMER => self::OPTION_FILTERS_CUSTOMER,
        User::ROLE_MANAGER => self::OPTION_FILTERS_MANAGER,
        User::ROLE_ADMIN => self::OPTION_FILTERS_ADMIN,
    ];

    private const REQUEST_PARAM_FILTER = 'filter';

    /**
     * @var Security
     */
    private $security;

    /**
     * @var ConfigRegisters
     */
    private $configRegisters;

    /**
     * @var Parser
     */
    private $parser;

    public function __construct(
        Security $security,
        ConfigRegisters $configRegisters,
        Parser $parser
    ) {
        $this->security = $security;
        $this->configRegisters = $configRegisters;
        $this->parser = $parser;
    }

    /**
     * @inheritdoc
     */
    public function supports(ParamConverter $configuration)
    {
        if (self::CONVERTER !== $configuration->getConverter()) {
            return false;
        }

        foreach (self::OPTIONS_FILTERS as $optionName) {
            $this->getFilters($configuration, $optionName);
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function apply(Request $request, ParamConverter $configuration)
    {
        /** @var User $user */
        $user = $this->security->getUser();

        if (!$user) {
            throw new \Exception('Filters are not supported for unauthenticated users');
        }

        $userRole = $user->getMainRole();

        if (!array_key_exists($userRole, self::OPTIONS_FILTERS)) {
            throw new \Exception('Unsupported user role: '.$userRole);
        }

        $filters = $this->getFilters($configuration, self::OPTIONS_FILTERS[$userRole]);
        $parsedFilter = $filters
            ? $this->parser->parse($filters, $this->getFilterString($request))
            : new Filter();

        $request->attributes->set($configuration->getName(), $parsedFilter);
    }

    /**
     * @param ParamConverter $configuration
     * @param string $optionName
     * @return ColumnRegistry|null
     */
    private function getFilters(ParamConverter $configuration, string $optionName): ?ColumnRegistry
    {
        if (!array_key_exists($optionName, $configuration->getOptions())) {
            return null;
        }

        $configurationName = $configuration->getOptions()[$optionName];

        if (!$this->configRegisters->hasRegistry($configurationName)) {
            throw new \InvalidArgumentException(sprintf(
                '%s - invalid configuration name: %s',
                $optionName,
                $configurationName
            ));
        }

        return $this->configRegisters->getRegistry($configurationName);
    }

    /**
     * @param Request $request
     * @return string
     */
    private function getFilterString(Request $request): string
    {
        return (string)$request->query->get(self::REQUEST_PARAM_FILTER, '');
    }
}
