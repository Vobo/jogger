<?php declare(strict_types=1);

namespace App\Service\ParamConverter\Filter\Parser;

use App\Manager\Query\Filter;
use App\Service\ParamConverter\Filter\Config\ColumnRegistry;
use App\Service\ParamConverter\Filter\Parser\Exception;
use App\Service\ParamConverter\Filter\Parser\FilterBuilder\FilterBuilder;
use App\Service\ParamConverter\Filter\Parser\LogicalOperation\LogicalOperationRegistry;
use App\Service\ParamConverter\Filter\Parser\Operator\OperatorRegistry;
use Nette\Tokenizer\Exception as TokenizerException;
use Nette\Tokenizer\Stream;
use Nette\Tokenizer\Token;
use Nette\Tokenizer\Tokenizer;

class Parser
{
    private const T_OPEN_BRACKET = 1;
    private const T_CLOSE_BRACKET = 2;
    private const T_WHITESPACE = 3;
    private const T_WORD = 4;
    private const T_SINGLE_QUOTE = 5;
    private const T_DOUBLE_QUOTE = 6;
    private const T_MINUS = 7;
    private const T_DOT = 8;
    private const T_SPECIAL_CHAR = 9;

    /**
     * @var OperatorRegistry
     */
    private $operatorRegistry;

    /**
     * @var LogicalOperationRegistry
     */
    private $logicalOperationsRegistry;

    /**
     * @var Tokenizer
     */
    private $tokenizer;

    public function __construct(
        OperatorRegistry $operatorRegistry,
        LogicalOperationRegistry $logicalOperationsRegistry
    ) {
        $this->operatorRegistry = $operatorRegistry;
        $this->logicalOperationsRegistry = $logicalOperationsRegistry;
        $this->tokenizer = new Tokenizer([
            self::T_OPEN_BRACKET => '\(',
            self::T_CLOSE_BRACKET => '\)',
            self::T_WHITESPACE => '\s+',
            self::T_WORD => '[a-z0-9]+',
            self::T_SINGLE_QUOTE => '\'',
            self::T_DOUBLE_QUOTE => '"',
            self::T_MINUS => '\-',
            self::T_DOT => '\.',
            self::T_SPECIAL_CHAR => '[_:]+',
        ], 'i');
    }

    /**
     * @param ColumnRegistry $columnConfigRegistry
     * @param string $filterString
     * @return Filter
     * @throws Exception\ParserException
     */
    public function parse(ColumnRegistry $columnConfigRegistry, string $filterString): Filter
    {
        try {
            $stream = $this->tokenizer->tokenize($filterString);
        } catch (TokenizerException $e) {
            throw new Exception\ParserException($e->getMessage(), 0, $e);
        }

        $filterBuilder = new FilterBuilder();
        $this->nextNonWhitespaceToken($stream);

        if ($stream->currentToken() && !$stream->isCurrent(self::T_OPEN_BRACKET)) {
            throw new Exception\TokenException('open bracket', $stream->currentToken());
        }

        while ($token = $stream->currentToken()) {
            if ($stream->isCurrent(self::T_WORD)) {
                $this->parseLogicalOperation($stream, $filterBuilder);
                $this->nextNonWhitespaceToken($stream);

                continue;
            }

            if (!$stream->isCurrent(self::T_OPEN_BRACKET)) {
                throw new Exception\TokenException('open bracket', $token);
            }

            $this->nextNonWhitespaceToken($stream);

            while ($stream->isCurrent(self::T_OPEN_BRACKET)) {
                $filterBuilder->enterContext();
                $this->nextNonWhitespaceToken($stream);
            }

            $this->parseComparison($stream, $filterBuilder, $columnConfigRegistry);
            $this->nextNonWhitespaceToken($stream);

            if (!$stream->isCurrent(self::T_CLOSE_BRACKET)) {
                throw $stream->currentToken()
                    ? new Exception\TokenException('closing bracket', $stream->currentToken())
                    : new Exception\EndOfStringException('closing bracket');
            }

            $this->nextNonWhitespaceToken($stream);

            while ($stream->isCurrent(self::T_CLOSE_BRACKET)) {
                $filterBuilder->leaveContext();
                $this->nextNonWhitespaceToken($stream);
            }
        }

        try {
            $filterBuilder->leaveContext();

            throw new Exception\EndOfStringException('closing bracket');
        } catch (Exception\BuilderException $e) {
            // Expected
        }

        return $filterBuilder->build();
    }

    /**
     * @param Stream $stream
     * @param FilterBuilder $filterBuilder
     * @param ColumnRegistry $columnConfigRegistry
     * @throws Exception\ParserException
     */
    private function parseComparison(Stream $stream, FilterBuilder $filterBuilder, ColumnRegistry $columnConfigRegistry)
    {
        $field = $stream->currentToken();

        if (!$stream->isCurrent(self::T_WORD)) {
            throw new Exception\TokenException('field', $field);
        }

        $operator = $this->nextNonWhitespaceToken($stream);

        if (!$stream->isCurrent(self::T_WORD)) {
            throw new Exception\TokenException('operator', $operator);
        }

        $this->nextNonWhitespaceToken($stream);
        $value = $this->getValue($stream);

        $operatorName = strtolower($operator->value);

        if (!$this->operatorRegistry->hasOperator($operatorName)) {
            throw new Exception\OperatorException($operator);
        }

        $fieldAlias = $field->value;

        if (!$columnConfigRegistry->hasColumn($fieldAlias)) {
            throw new Exception\FieldException($fieldAlias);
        }

        $fieldConfig = $columnConfigRegistry->getColumn($fieldAlias);

        if (!$fieldConfig->isCanFilterBy()) {
            throw new Exception\FieldException($fieldAlias);
        }

        $this->operatorRegistry
            ->getOperator($operatorName)
            ->handleOperation($filterBuilder, $fieldConfig->getSqlName(), $value);
    }

    /**
     * @param Stream $stream
     * @return string
     * @throws Exception\ParserException
     */
    private function getValue(Stream $stream): string
    {
        $token = $stream->currentToken();
        $isNegative = false;

        // Handle quoted strings
        if ($isDoubleQuote = $stream->isCurrent(self::T_SINGLE_QUOTE) || $stream->isCurrent(self::T_DOUBLE_QUOTE)) {
            $stream->nextToken();

            if ($stream->isCurrent($token->type)) {
                return '';
            }

            $value = $stream->currentValue() . $stream->joinUntil($token->type);
            $stream->nextToken();

            if (!$stream->isCurrent($token->type)) {
                throw new Exception\EndOfStringException($isDoubleQuote ? 'double quote' : 'single quote');
            }

            return $value;
        }

        if ($stream->isCurrent(self::T_MINUS)) {
            $isNegative = true;
            $stream->nextToken();
        }

        if (!$stream->isCurrent(self::T_WORD)) {
            throw new Exception\TokenException('value', $token);
        }

        $value = $stream->currentValue();

        if ($stream->isNext(self::T_DOT)) {
            $stream->nextToken();
            $value .= '.';
            $stream->nextToken();

            if (!$stream->isCurrent(self::T_WORD)) {
                throw new Exception\TokenException('value', $token);
            }

            $value .= $stream->currentValue();
            $value = (float)$value;
        }

        if ($isNegative) {
            $value *= -1;
        }

        return (string)$value;
    }

    /**
     * @param Stream $stream
     * @param FilterBuilder $filterBuilder
     * @throws Exception\LogicalOperationException
     */
    private function parseLogicalOperation(Stream $stream, FilterBuilder $filterBuilder)
    {
        $logicalOperationName = strtolower($stream->currentValue());

        if (!$this->logicalOperationsRegistry->hasOperation($logicalOperationName)) {
            throw new Exception\LogicalOperationException($stream->currentToken());
        }

        $this->logicalOperationsRegistry
            ->getOperation($logicalOperationName)
            ->handleOperation($filterBuilder);
    }

    /**
     * @param Stream $stream
     * @return Token|null
     */
    private function nextNonWhitespaceToken(Stream $stream): ?Token
    {
        do {
            $stream->nextToken();
        } while($stream->isCurrent(self::T_WHITESPACE));

        return $stream->currentToken();
    }
}
