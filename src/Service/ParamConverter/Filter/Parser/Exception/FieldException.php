<?php declare(strict_types=1);

namespace App\Service\ParamConverter\Filter\Parser\Exception;

class FieldException extends ParserException
{
    public function __construct(string $fieldName)
    {
        parent::__construct(sprintf(
            'Cannot filter by "%s".',
            $fieldName
        ));
    }
}
