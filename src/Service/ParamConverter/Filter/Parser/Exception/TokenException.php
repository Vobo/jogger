<?php declare(strict_types=1);

namespace App\Service\ParamConverter\Filter\Parser\Exception;

use Nette\Tokenizer\Token;

class TokenException extends ParserException
{
    /**
     * @param string $expected Value
     * @param Token $got
     */
    public function __construct(string $expected, ?Token $got)
    {
        parent::__construct(sprintf(
            'Expected %s, got "%s" at %d',
            $expected,
            $got->value,
            $got->offset
        ));
    }
}
