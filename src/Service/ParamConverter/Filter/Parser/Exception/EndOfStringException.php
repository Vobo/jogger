<?php declare(strict_types=1);

namespace App\Service\ParamConverter\Filter\Parser\Exception;

class EndOfStringException extends ParserException
{
    public function __construct(string $expected)
    {
        parent::__construct(sprintf(
            'End of string, expected %s',
            $expected
        ));
    }
}
