<?php declare(strict_types=1);

namespace App\Service\ParamConverter\Filter\Parser\Exception;

class BuilderException extends ParserException
{

}
