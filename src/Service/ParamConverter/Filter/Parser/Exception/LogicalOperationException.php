<?php declare(strict_types=1);

namespace App\Service\ParamConverter\Filter\Parser\Exception;

use Nette\Tokenizer\Token;

class LogicalOperationException extends ParserException
{
    /**
     * @param Token $operation
     */
    public function __construct(Token $operation)
    {
        parent::__construct(sprintf(
            'Unexpected logical operation "%s" at %d',
            $operation->value,
            $operation->offset
        ));
    }
}
