<?php declare(strict_types=1);

namespace App\Service\ParamConverter\Filter\Parser\LogicalOperation;

class LogicalOperationRegistry
{
    /**
     * @var LogicalOperationInterface[]
     */
    private $operations = [];

    /**
     * @param LogicalOperationInterface[] $operations
     */
    public function __construct(array $operations)
    {
        foreach ($operations as $operation) {
            $this->addOperation($operation);
        }
    }

    /**
     * @param string $operationName
     * @return bool
     */
    public function hasOperation(string $operationName): bool
    {
        return array_key_exists($operationName, $this->operations);
    }

    /**
     * @param string $operationName
     * @return LogicalOperationInterface
     */
    public function getOperation(string $operationName): LogicalOperationInterface
    {
        if (!$this->hasOperation($operationName)) {
            throw new \InvalidArgumentException(sprintf(
                'No operation named "%s" exists',
                $operationName
            ));
        }

        return $this->operations[$operationName];
    }

    /**
     * @param LogicalOperationInterface $operation
     */
    private function addOperation(LogicalOperationInterface $operation)
    {
        $operationName = $operation->getOperationName();

        if ($this->hasOperation($operationName)) {
            throw new \InvalidArgumentException(sprintf(
                'Another operation with the name "%s" registered',
                $operationName)
            );
        }

        $this->operations[$operation->getOperationName()] = $operation;
    }
}
