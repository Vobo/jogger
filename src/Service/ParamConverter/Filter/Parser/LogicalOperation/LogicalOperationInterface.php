<?php declare(strict_types=1);

namespace App\Service\ParamConverter\Filter\Parser\LogicalOperation;

use App\Service\ParamConverter\Filter\Parser\FilterBuilder\FilterBuilder;

interface LogicalOperationInterface
{
    /**
     * @return string
     */
    public function getOperationName(): string;

    /**
     * @param FilterBuilder $filterBuilder
     */
    public function handleOperation(FilterBuilder $filterBuilder): void;
}
