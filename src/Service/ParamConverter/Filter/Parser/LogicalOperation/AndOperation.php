<?php declare(strict_types=1);

namespace App\Service\ParamConverter\Filter\Parser\LogicalOperation;

use App\Service\ParamConverter\Filter\Parser\FilterBuilder\FilterBuilder;

class AndOperation implements LogicalOperationInterface
{
    private const OPERATION = 'and';

    /**
     * @inheritdoc
     */
    public function getOperationName(): string
    {
        return self::OPERATION;
    }

    /**
     * @inheritdoc
     */
    public function handleOperation(FilterBuilder $filterBuilder): void
    {
        $filterBuilder->setAnd();
    }
}
