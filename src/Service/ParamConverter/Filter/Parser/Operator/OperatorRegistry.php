<?php declare(strict_types=1);

namespace App\Service\ParamConverter\Filter\Parser\Operator;

class OperatorRegistry
{
    /**
     * @var OperatorInterface[]
     */
    private $operators = [];

    /**
     * @param OperatorInterface[] $operators
     */
    public function __construct(array $operators)
    {
        foreach ($operators as $operator) {
            $this->addOperator($operator);
        }
    }

    /**
     * @param string $operatorName
     * @return bool
     */
    public function hasOperator(string $operatorName): bool
    {
        return array_key_exists($operatorName, $this->operators);
    }

    /**
     * @param string $operatorName
     * @return OperatorInterface
     */
    public function getOperator(string $operatorName): OperatorInterface
    {
        if (!$this->hasOperator($operatorName)) {
            throw new \InvalidArgumentException(sprintf(
                'No operator named "%s" exists',
                $operatorName
            ));
        }

        return $this->operators[$operatorName];
    }

    /**
     * @param OperatorInterface $operator
     */
    private function addOperator(OperatorInterface $operator): void
    {
        $operatorName = $operator->getOperatorName();

        if ($this->hasOperator($operatorName)) {
            throw new \InvalidArgumentException(sprintf(
                'Another operator with the name "%s" registered',
                $operatorName
            ));
        }

        $this->operators[$operatorName] = $operator;
    }
}
