<?php declare(strict_types=1);

namespace App\Service\ParamConverter\Filter\Parser\Operator;

use App\Service\ParamConverter\Filter\Parser\FilterBuilder\FilterBuilder;
use Doctrine\ORM\Query\Expr;

class LikeOperator implements OperatorInterface
{
    private const OPERATOR = 'LIKE';

    /**
     * @var string
     */
    protected $operatorName;

    public function __construct(string $operatorName)
    {
        $this->operatorName = $operatorName;
    }

    /**
     * @inheritdoc
     */
    public function getOperatorName(): string
    {
        return $this->operatorName;
    }

    /**
     * @inheritdoc
     */
    public function handleOperation(FilterBuilder $filterBuilder, string $column, $value): void
    {
        $value = '%'.$value.'%';
        $filterBuilder->add(new Expr\Comparison(
            $column,
            self::OPERATOR,
            ':'.$filterBuilder->addParam($value)
        ));
    }
}
