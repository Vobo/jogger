<?php declare(strict_types=1);

namespace App\Service\ParamConverter\Filter\Parser\Operator;

use App\Service\ParamConverter\Filter\Parser\FilterBuilder\FilterBuilder;

interface OperatorInterface
{
    /**
     * @return string
     */
    public function getOperatorName(): string;

    /**
     * @param FilterBuilder $filterBuilder
     * @param string $column
     * @param mixed $value
     */
    public function handleOperation(FilterBuilder $filterBuilder, string $column, $value): void;
}
