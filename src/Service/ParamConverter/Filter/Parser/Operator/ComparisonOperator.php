<?php declare(strict_types=1);

namespace App\Service\ParamConverter\Filter\Parser\Operator;

use App\Service\ParamConverter\Filter\Parser\FilterBuilder\FilterBuilder;
use Doctrine\ORM\Query\Expr;

class ComparisonOperator implements OperatorInterface
{
    /**
     * @var string
     */
    private $operatorName;

    /**
     * @var string
     */
    private $sqlOperator;

    public function __construct(string $operatorName, string $sqlOperator)
    {
        $this->operatorName = $operatorName;
        $this->sqlOperator = $sqlOperator;
    }

    /**
     * @inheritdoc
     */
    public function getOperatorName(): string
    {
        return $this->operatorName;
    }

    /**
     * @inheritdoc
     */
    public function handleOperation(FilterBuilder $filterBuilder, string $column, $value): void
    {
        $filterBuilder->add(new Expr\Comparison(
            $column,
            $this->sqlOperator,
            ':'.$filterBuilder->addParam($value)
        ));
    }
}
