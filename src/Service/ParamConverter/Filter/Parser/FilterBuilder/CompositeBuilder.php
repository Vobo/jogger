<?php declare(strict_types=1);

namespace App\Service\ParamConverter\Filter\Parser\FilterBuilder;

use App\Service\ParamConverter\Filter\Parser\Exception\BuilderException;
use Doctrine\ORM\Query\Expr;

class CompositeBuilder
{
    public const TYPE_AND = 'and';
    public const TYPE_OR = 'or';

    /**
     * @var CompositeBuilder|null
     */
    private $parent;

    /**
     * @var string|null
     */
    private $type;

    /**
     * @var array
     */
    private $children = [];

    public function __construct(CompositeBuilder $parent = null)
    {
        $this->parent = $parent;
    }

    /**
     * @return CompositeBuilder|null
     */
    public function getParent(): ?CompositeBuilder
    {
        return $this->parent;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @return CompositeBuilder
     * @throws BuilderException
     */
    public function setAnd(): self
    {
        if ($this->type && self::TYPE_AND !== $this->type) {
            throw new BuilderException('Cannot change once set type');
        }

        $this->type = self::TYPE_AND;

        return $this;
    }

    /**
     * @return CompositeBuilder
     * @throws BuilderException
     */
    public function setOr(): self
    {
        if ($this->type && self::TYPE_OR !== $this->type) {
            throw new BuilderException('Cannot change once set type');
        }

        $this->type = self::TYPE_OR;

        return $this;
    }

    /**
     * @param Expr\Composite|Expr\Comparison $child
     * @return CompositeBuilder
     * @throws BuilderException
     */
    public function addChild($child): self
    {
        if (!$child instanceof Expr\Composite && !$child instanceof Expr\Comparison) {
            throw new BuilderException('Child not supported: '.(is_object($child) ? get_class($child) : gettype($child)));
        }

        $this->children[] = $child;

        return $this;
    }

    /**
     * @return Expr\Composite
     * @throws BuilderException
     */
    public function build(): Expr\Composite
    {
        switch ($this->type ?? self::TYPE_AND) {
            case self::TYPE_AND:
                return new Expr\Andx($this->children);

            case self::TYPE_OR:
                return new Expr\Orx($this->children);

            default:
                throw new BuilderException('Unsupported context type: '.$this->type);
        }
    }
}
