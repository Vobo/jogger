<?php declare(strict_types=1);

namespace App\Service\ParamConverter\Filter\Parser\FilterBuilder;

use App\Manager\Query\Filter;
use App\Service\ParamConverter\Filter\Parser\Exception\BuilderException;
use Doctrine\ORM\Query\Expr;

class FilterBuilder
{
    /**
     * @var string
     */
    private $paramPrefix;

    /**
     * @var CompositeBuilder
     */
    private $context;

    /**
     * @var array
     */
    private $params = [];

    public function __construct(string $paramPrefix = 'param')
    {
        $this->paramPrefix = $paramPrefix;
        $this->context = new CompositeBuilder();
    }

    /**
     * @return $this
     */
    public function enterContext(): self
    {
        $parent = $this->context;
        $this->context = new CompositeBuilder($parent);

        return $this;
    }

    /**
     * @return $this
     * @throws BuilderException When there is no
     */
    public function leaveContext(): self
    {
        $parent = $this->context->getParent();

        if (!$parent) {
            throw new BuilderException('Cannot leave last context');
        }

        $node = $this->context->build();
        $this->context = $parent;
        $this->context->addChild($node);

        return $this;
    }

    /**
     * @return $this
     * @throws BuilderException
     */
    public function setAnd(): self
    {
        $this->context->setAnd();

        return $this;
    }

    /**
     * @return $this
     * @throws BuilderException
     */
    public function setOr(): self
    {
        $this->context->setOr();

        return $this;
    }

    /**
     * @param Expr\Comparison $node
     * @return $this
     */
    public function add(Expr\Comparison $node): self
    {
        $this->context->addChild($node);

        return $this;
    }

    /**
     * Create a param
     *
     * @param mixed $value
     * @return string Param name
     */
    public function addParam($value): string
    {
        $key = sprintf('%s_%d', $this->paramPrefix, count($this->params));
        $this->params[$key] = $value;

        return $key;
    }

    /**
     * @return Filter
     * @throws BuilderException
     */
    public function build(): Filter
    {
        while ($this->context->getParent()) {
            $this->leaveContext();
        }

        return new Filter(
            $this->context->build(),
            $this->params
        );
    }
}
