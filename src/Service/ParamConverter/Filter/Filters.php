<?php declare(strict_types=1);

namespace App\Service\ParamConverter\Filter;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * @Annotation
 */
class Filters extends ParamConverter
{
    /**
     * @inheritdoc
     */
    public function getConverter()
    {
        return FiltersConverter::CONVERTER;
    }
}
