<?php declare(strict_types=1);

namespace App\Service\ParamConverter\Filter\Config;

class Column
{
    /**
     * @var string
     */
    private $alias;

    /**
     * @var string
     */
    private $sqlName;

    /**
     * @var bool
     */
    private $canFilterBy;

    /**
     * @var bool
     */
    private $canSortBy;

    public function __construct(string $alias, string $sqlName, bool $canFilterBy, bool $canSortBy)
    {
        $this->alias = $alias;
        $this->sqlName = $sqlName;
        $this->canFilterBy = $canFilterBy;
        $this->canSortBy = $canSortBy;
    }

    /**
     * @return string
     */
    public function getAlias(): string
    {
        return $this->alias;
    }

    /**
     * @return string
     */
    public function getSqlName(): string
    {
        return $this->sqlName;
    }

    /**
     * @return bool
     */
    public function isCanFilterBy(): bool
    {
        return $this->canFilterBy;
    }

    /**
     * @return bool
     */
    public function isCanSortBy(): bool
    {
        return $this->canSortBy;
    }
}
