<?php declare(strict_types=1);

namespace App\Service\ParamConverter\Filter\Config;

class ColumnRegistry
{
    /**
     * @var Column[]
     */
    private $columns = [];

    /**
     * @param Column[] $columns
     */
    public function __construct(array $columns)
    {
        foreach ($columns as $column) {
            $this->addColumn($column);
        }
    }

    /**
     * @param string $alias
     * @return bool
     */
    public function hasColumn(string $alias): bool
    {
        return array_key_exists($alias, $this->columns);
    }

    /**
     * @param string $alias
     * @return Column
     */
    public function getColumn(string $alias): Column
    {
        if (!$this->hasColumn($alias)) {
            throw new \InvalidArgumentException(sprintf(
                'No column with the alias "%s" registered',
                $alias
            ));
        }

        return $this->columns[$alias];
    }

    /**
     * @param Column $column
     */
    private function addColumn(Column $column): void
    {
        $alias = $column->getAlias();

        if ($this->hasColumn($alias)) {
            throw new \InvalidArgumentException(sprintf(
                'Another column with the alias "%s" registered',
                $alias
            ));
        }

        $this->columns[$alias] = $column;
    }
}
