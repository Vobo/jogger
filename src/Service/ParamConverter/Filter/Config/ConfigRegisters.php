<?php declare(strict_types=1);

namespace App\Service\ParamConverter\Filter\Config;

class ConfigRegisters
{
    /**
     * @var ColumnRegistry[]
     */
    private $registers = [];

    /**
     * @param ColumnRegistry[] $columnRegisters
     */
    public function __construct(array $columnRegisters)
    {
        foreach ($columnRegisters as $action => $register) {
            $this->addRegistry($action, $register);
        }
    }

    /**
     * @param $actionName
     * @return bool
     */
    public function hasRegistry(string $actionName): bool
    {
        return array_key_exists($actionName, $this->registers);
    }

    /**
     * @param string $actionName
     * @return ColumnRegistry
     */
    public function getRegistry(string $actionName): ColumnRegistry
    {
        if (!$this->hasRegistry($actionName)) {
            throw new \InvalidArgumentException(sprintf(
                'No registry for %s action',
                $actionName
            ));
        }

        return $this->registers[$actionName];
    }

    /**
     * @param string $actionName
     * @param ColumnRegistry $config
     */
    private function addRegistry(string $actionName, ColumnRegistry $config)
    {
        if ($this->hasRegistry($actionName)) {
            throw new \InvalidArgumentException(sprintf(
                'Another registry for %s action exists already',
                $actionName
            ));
        }

        $this->registers[$actionName] = $config;
    }
}
