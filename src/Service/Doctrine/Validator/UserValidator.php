<?php declare(strict_types=1);

namespace App\Service\Doctrine\Validator;

use App\Entity\User;
use App\Manager\Exception\NotFoundException;
use App\Manager\UserManager;
use App\Service\Doctrine\Validator\User as UserConstraint;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class UserValidator extends ConstraintValidator
{
    /**
     * @var UserManager
     */
    private $userManager;

    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @inheritdoc
     * @param UserConstraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof UserConstraint) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__.'\User');
        }

        // custom constraints should ignore null and empty values to allow
        // other constraints (NotBlank, NotNull, etc.) take care of that
        if (null === $value || '' === $value) {
            return;
        }

        if (!is_int($value)) {
            throw new UnexpectedTypeException($value, 'int');
        }

        try {
            $user = $this->userManager->getUser($value);
        } catch (NotFoundException $e) {
            $this->context
                ->buildViolation($constraint->messageUserNotFound)
                ->setParameter('{{ user }}', $value)
                ->addViolation();

            return;
        }

        if (!in_array(User::ROLE_CUSTOMER, $user->getRoles())) {
            $this->context
                ->buildViolation($constraint->messageInvalidRole)
                ->addViolation();
        }
    }
}
