<?php declare(strict_types=1);

namespace App\Service\Doctrine\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class User extends Constraint
{
    public $messageUserNotFound = 'User {{ user }} not found.';
    public $messageInvalidRole = 'Invalid role.';
}
