<?php declare(strict_types=1);

namespace App\Service\Doctrine\Validator;

use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class WhenRoleValidator extends ConstraintValidator
{
    /**
     * @var Security
     */
    private $security;

    public function __construct(
        Security $security
    ) {
        $this->security = $security;
    }

    /**
     * @inheritdoc
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof WhenRole) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__ . '\WhenRole');
        }

        $user = $this->security->getUser();

        if (!$user || !in_array($constraint->role, $user->getRoles())) {
            return;
        }

        $context = $this->context;
        $validator = $context->getValidator()->inContext($context);
        $validator->validate($value, $constraint->constraints);
    }
}
