<?php declare(strict_types=1);

namespace App\Service\Doctrine\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class Unique extends Constraint
{
    public $message = 'Value taken.';

    public $field;

    public $class;

    public $routeIdParam = 'id';
}
