<?php declare(strict_types=1);

namespace App\Service\Doctrine\Validator;

use App\Service\Doctrine\Validator\User as UserConstraint;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class UniqueValidator extends ConstraintValidator
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var RequestStack
     */
    private $requestStack;

    public function __construct(
        EntityManagerInterface $entityManager,
        RequestStack $requestStack
    ) {
        $this->entityManager = $entityManager;
        $this->requestStack = $requestStack;
    }

    /**
     * @inheritdoc
     * @param UserConstraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof Unique) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__.'\Unique');
        }

        // custom constraints should ignore null and empty values to allow
        // other constraints (NotBlank, NotNull, etc.) take care of that
        if (null === $value || '' === $value) {
            return;
        }

        $repository = $this->entityManager->getRepository($constraint->class);
        $results = $repository->findBy([$constraint->field => $value]);
        $currentId = $this->getObjectId($constraint);

        foreach ($results as $result) {
            $id = $result->getId();

            if ($id !== $currentId) {
                $this->context
                    ->buildViolation($constraint->message)
                    ->addViolation();
            }
        }
    }

    /**
     * Get validated object id from request param
     *
     * @param Unique $constraint
     * @return int|null
     */
    private function getObjectId(Unique $constraint): ?int
    {
        $controllerAttributes = $this->requestStack->getMasterRequest()->attributes;

        if (!$controllerAttributes->has($constraint->routeIdParam)) {
            return null;
        }

        return (int)$controllerAttributes->get($constraint->routeIdParam);
    }
}
