<?php declare(strict_types=1);

namespace App\Service\Doctrine\Validator;

use Symfony\Component\Validator\Constraints\Composite;

/**
 * @Annotation
 */
class WhenRole extends Composite
{
    public $role;

    public $constraints = [];

    public function getDefaultOption()
    {
        return 'constraints';
    }

    public function getRequiredOptions()
    {
        return ['constraints'];
    }

    protected function getCompositeOption()
    {
        return 'constraints';
    }
}
