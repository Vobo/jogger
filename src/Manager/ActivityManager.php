<?php declare(strict_types=1);

namespace App\Manager;

use App\Entity\Activity;
use App\Manager\Exception\NotFoundException;
use App\Manager\Query\Query;
use App\Manager\Query\Result;
use App\Repository\ActivityRepository;
use App\Repository\WeatherRepository;
use Doctrine\Common\Persistence\ObjectManager;

class ActivityManager
{
    /**
     * @var ActivityRepository
     */
    private $activityRepository;

    /**
     * @var WeatherRepository
     */
    private $weatherRepository;

    /**
     * @var ObjectManager
     */
    private $objectManager;

    public function __construct(
        ActivityRepository $activityRepository,
        WeatherRepository $weatherRepository,
        ObjectManager $objectManager
    ) {
        $this->activityRepository = $activityRepository;
        $this->weatherRepository = $weatherRepository;
        $this->objectManager = $objectManager;
    }

    /**
     * @param int $id
     * @return Activity
     * @throws NotFoundException
     */
    public function getActivity(int $id): Activity
    {
        $activity = $this->activityRepository->get($id);

        if (!$activity) {
            throw new NotFoundException(sprintf('Activity %d not found', $id));
        }

        return $activity;
    }

    /**
     * @param Query $query
     * @return Result
     */
    public function queryActivities(Query $query): Result
    {
        $count = $this->activityRepository->count($query->getFilter());
        $entities = $this->activityRepository->query($query);

        return new Result($count, $query->getPage(), $query->getSize(), $entities);
    }

    /**
     * @param Activity $activity
     * @return Activity
     */
    public function createActivity(Activity $activity): Activity
    {
        if (null !== $activity->getId()) {
            throw new \InvalidArgumentException('Activity already exists, update instead');
        }

        $this->updateWeather($activity);

        $this->objectManager->persist($activity);
        $this->objectManager->flush();

        return $activity;
    }

    /**
     * @param Activity $activity
     * @return Activity
     */
    public function updateActivity(Activity $activity): Activity
    {
        $this->updateWeather($activity);

        $this->objectManager->persist($activity);
        $this->objectManager->flush();

        return $activity;
    }

    /**
     * @param Activity $activity
     */
    public function deleteActivity(Activity $activity): void
    {
        $this->objectManager->remove($activity);
        $this->objectManager->flush();
    }

    /**
     * @param Activity $activity
     */
    private function updateWeather(Activity $activity): void
    {
        $weather = $this->weatherRepository->getWeather(
            $activity->getDate(),
            $activity->getLocation()
        );
        $activity
            ->setTemperature($weather->getTemperature())
            ->setWindSpeed($weather->getWindSpeed());
    }
}
