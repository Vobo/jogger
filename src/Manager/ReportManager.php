<?php declare(strict_types=1);

namespace App\Manager;

use App\Manager\Query\Query;
use App\Manager\Query\Result;
use App\Repository\StatisticsRepository;

class ReportManager
{
    /**
     * @var StatisticsRepository
     */
    private $statisticsRepository;

    public function __construct(
        StatisticsRepository $statisticsRepository
    ) {
        $this->statisticsRepository = $statisticsRepository;
    }

    /**
     * @param Query $query
     * @return Result
     */
    public function queryStatistics(Query $query): Result
    {
        $count = $this->statisticsRepository->count($query->getFilter());
        $entities = $this->statisticsRepository->query($query);

        return new Result($count, $query->getPage(), $query->getSize(), $entities);
    }
}
