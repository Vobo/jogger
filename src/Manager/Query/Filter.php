<?php declare(strict_types=1);

namespace App\Manager\Query;

use Doctrine\ORM\Query\Expr;

class Filter
{
    /**
     * @var Expr\Composite
     */
    private $where;

    /**
     * @var array
     */
    private $parameters;

    /**
     * @param Expr\Composite|null $where
     * @param array $parameters
     */
    public function __construct(Expr\Composite $where = null, array $parameters = [])
    {
        $this->where = new Expr\Andx();
        $this->parameters = $parameters;

        if ($where) {
            $this->where->add($where);
        }
    }

    /**
     * @return Expr\Andx|Expr\Orx
     */
    public function getWhere(): Expr\Composite
    {
        return $this->where;
    }

    /**
     * @return array
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }
}
