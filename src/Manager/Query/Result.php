<?php declare(strict_types=1);

namespace App\Manager\Query;

class Result
{
    /**
     * @var int
     */
    private $count;

    /**
     * @var int
     */
    private $page;

    /**
     * @var int
     */
    private $size;

    /**
     * @var array
     */
    private $results;

    public function __construct(
        int $count,
        int $page,
        int $size,
        array $result
    ) {
        $this->count = $count;
        $this->page = $page;
        $this->size = $size;
        $this->results = $result;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function getSize(): int
    {
        return $this->size;
    }

    /**
     * @return array
     */
    public function getResults(): array
    {
        return $this->results;
    }

    /**
     * @return int
     */
    public function getPages(): int
    {
        return (int)ceil($this->count / $this->size);
    }
}
