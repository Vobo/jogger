<?php declare(strict_types=1);

namespace App\Manager\Query;

class Query
{
    /**
     * @var int
     */
    private $page;

    /**
     * @var int
     */
    private $size;

    /**
     * @var Filter
     */
    private $filter;

    /**
     * @param int $page First page = 1
     * @param int $size
     * @param Filter $filter
     */
    public function __construct(
        int $page = 1,
        int $size = 10,
        Filter $filter
    ) {
        $this->page = $page;
        $this->size = $size;
        $this->filter = $filter;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function getSize(): int
    {
        return $this->size;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        return ($this->page - 1) * $this->size;
    }

    /**
     * @return Filter
     */
    public function getFilter(): Filter
    {
        return $this->filter;
    }
}
