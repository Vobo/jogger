<?php declare(strict_types=1);

namespace App\Manager\Exception;

class NotFoundException extends ManagerException
{

}
