<?php declare(strict_types=1);

namespace App\Manager;

use App\Entity\User;
use App\Manager\Exception\NotFoundException;
use App\Manager\Query\Query;
use App\Manager\Query\Result;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use FOS\UserBundle\Doctrine\UserManager as BaseUserManager;

class UserManager
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * @var BaseUserManager
     */
    private $baseUserManager;

    public function __construct(
        UserRepository $userRepository,
        ObjectManager $objectManager,
        BaseUserManager $baseUserManager
    ) {
        $this->userRepository = $userRepository;
        $this->objectManager = $objectManager;
        $this->baseUserManager = $baseUserManager;
    }

    /**
     * @param int $id
     * @return User
     * @throws NotFoundException
     */
    public function getUser(int $id): User
    {
        $user = $this->userRepository->get($id);

        if (!$user) {
            throw new NotFoundException(sprintf('User %d not found', $id));
        }

        return $user;
    }

    /**
     * @param Query $query
     * @return Result
     */
    public function queryUsers(Query $query): Result
    {
        $count = $this->userRepository->count($query->getFilter());
        $entities = $this->userRepository->query($query);

        return new Result($count, $query->getPage(), $query->getSize(), $entities);
    }

    /**
     * @param User $user
     * @return User
     */
    public function createUser(User $user): User
    {
        if (null !== $user->getId()) {
            throw new \InvalidArgumentException('User already exists, update instead');
        }

        $user->setEnabled(true);
        $this->baseUserManager->updateUser($user);

        return $user;
    }

    /**
     * @param User $user
     * @return User
     */
    public function updateUser(User $user): User
    {
        $this->baseUserManager->updateUser($user);

        return $user;
    }

    /**
     * @param User $user
     */
    public function deleteUser(User $user): void
    {
        $this->baseUserManager->deleteUser($user);
    }
}
