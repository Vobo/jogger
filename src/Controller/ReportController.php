<?php declare(strict_types=1);

namespace App\Controller;

use App\DTO\Report\ReadReportCollectionDTO;
use App\Entity\User;
use App\Manager\Query\Filter;
use App\Manager\Query\Query;
use App\Manager\ReportManager;
use App\Transformer\ReportTransformer;
use App\Service\ParamConverter\Filter\Filters;
use Doctrine\ORM\Query\Expr\Comparison;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Component\Security\Core\Security;

/**
 * @Rest\Route("/report")
 */
class ReportController
{
    /**
     * @var Security
     */
    private $security;

    /**
     * @var ReportManager
     */
    private $reportManager;

    /**
     * @var ReportTransformer
     */
    private $reportTransformer;

    public function __construct(
        Security $security,
        ReportManager $reportManager,
        ReportTransformer $reportTransformer
    ) {
        $this->security = $security;
        $this->reportManager = $reportManager;
        $this->reportTransformer = $reportTransformer;
    }

    /**
     * @Rest\Get("/statistics")
     * @Rest\View()
     *
     * @Rest\QueryParam(name="page", requirements="[1-9]+\d*", default="1")
     * @Rest\QueryParam(name="limit", requirements="[1-9]+\d*", default="10")
     *
     * @Filters("filter", options={
     *     "customer": "report.statistics.customer",
     *     "admin": "report.statistics.admin"
     * })
     *
     * @param ParamFetcherInterface $paramFetcher
     * @param Filter $filter
     * @return ReadReportCollectionDTO
     */
    public function getStatisticsAction(ParamFetcherInterface $paramFetcher, Filter $filter)
    {
        if (!$this->security->isGranted(User::ROLE_ADMIN)) {
            /** @var User $user */
            $user = $this->security->getUser();
            $filter->getWhere()->add(new Comparison('x.user', Comparison::EQ, $user->getId()));
        }

        $query = new Query(
            (int)$paramFetcher->get('page'),
            (int)$paramFetcher->get('limit'),
            $filter
        );
        $result = $this->reportManager->queryStatistics($query);

        return $this->reportTransformer->resultToModelCollection($result);
    }
}
