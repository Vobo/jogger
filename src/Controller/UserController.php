<?php declare(strict_types=1);

namespace App\Controller;

use App\DTO\User\ReadUserCollectionDTO;
use App\DTO\User\ReadUserDTO;
use App\DTO\User\WriteUserDTO;
use App\Entity\User;
use App\Manager\Exception\NotFoundException;
use App\Manager\Query\Filter;
use App\Manager\Query\Query;
use App\Manager\UserManager;
use App\Transformer\UserTransformer;
use App\Service\ParamConverter\Filter\Filters;
use Doctrine\ORM\Query\Expr\Comparison;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Rest\Route("/user")
 */
class UserController
{
    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var UserTransformer
     */
    private $userTransformer;

    /**
     * @var Security
     */
    private $security;

    public function __construct(
        UserManager $userManager,
        UserTransformer $userTransformer,
        Security $security
    ) {
        $this->userManager = $userManager;
        $this->userTransformer = $userTransformer;
        $this->security = $security;
    }

    /**
     * @Rest\Get()
     * @Rest\View()
     *
     * @Rest\QueryParam(name="page", requirements="[1-9]+\d*", default="1")
     * @Rest\QueryParam(name="limit", requirements="[1-9]+\d*", default="10")
     *
     * @Filters("filter", options={
     *     "customer": "user",
     *     "manager": "user",
     *     "admin": "user"
     * })
     *
     * @param ParamFetcherInterface $paramFetcher
     * @param Filter $filter
     * @return ReadUserCollectionDTO
     */
    public function getListAction(ParamFetcherInterface $paramFetcher, Filter $filter)
    {
        $query = new Query(
            (int)$paramFetcher->get('page'),
            (int)$paramFetcher->get('limit'),
            $filter
        );
        $result = $this->userManager->queryUsers($query);

        return $this->userTransformer->resultToModelCollection($result);
    }

    /**
     * @Rest\Get("/{userId}")
     * @Rest\View()
     *
     * @param int $userId
     * @return ReadUserDTO
     * @throws NotFoundException
     */
    public function getAction(int $userId)
    {
        $user = $this->userManager->getUser($userId);

        return $this->userTransformer->entityToModel($user);
    }

    /**
     * @Rest\Post()
     * @Rest\View(statusCode=201)
     * @ParamConverter("model", converter="fos_rest.request_body", options={"validator"={"groups"={"Default", "Create"}}})
     *
     * @param WriteUserDTO $model
     * @param ConstraintViolationListInterface $validationErrors
     * @return ReadUserDTO|View
     */
    public function createAction(WriteUserDTO $model, ConstraintViolationListInterface $validationErrors)
    {
        if (!$this->security->isGranted(User::ROLE_MANAGER)) {
            $model->setRole(User::ROLE_CUSTOMER);
        }

        if (count($validationErrors)) {
            return new View($validationErrors, 400);
        }

        $user = $this->userTransformer->modelToEntity($model);
        $user = $this->userManager->createUser($user);
        $model = $this->userTransformer->entityToModel($user);

        return $model;
    }

    /**
     * @Rest\Put("/{userId}")
     * @Rest\View()
     * @ParamConverter("model", converter="fos_rest.request_body" )
     *
     * @param int $userId
     * @param WriteUserDTO $model
     * @param ConstraintViolationListInterface $validationErrors
     * @return ReadUserDTO|View
     * @throws NotFoundException
     */
    public function updateAction(int $userId, WriteUserDTO $model, ConstraintViolationListInterface $validationErrors)
    {
        $user = $this->userManager->getUser($userId);

        if (!$this->security->isGranted('owner', $user)) {
            throw new NotFoundHttpException('User not found');
        }

        if (count($validationErrors)) {
            return new View($validationErrors, 400);
        }

        $model->setPassword(null);
        $user = $this->userTransformer->modelToEntity($model, $user);
        $user = $this->userManager->updateUser($user);
        $model = $this->userTransformer->entityToModel($user);

        return $model;
    }

    /**
     * @Rest\Delete("/{userId}")
     * @Rest\View(statusCode=204)
     *
     * @param int $userId
     * @throws NotFoundException
     */
    public function deleteAction(int $userId)
    {
        $user = $this->userManager->getUser($userId);

        if (!$this->security->isGranted('owner', $user)) {
            throw new NotFoundHttpException('User not found');
        }

        $this->userManager->deleteUser($user);
    }
}
