<?php declare(strict_types=1);

namespace App\Controller;

use App\DTO\Activity\ReadActivityCollectionDTO;
use App\DTO\Activity\ReadActivityDTO;
use App\DTO\Activity\WriteActivityDTO;
use App\Entity\User;
use App\Manager\ActivityManager;
use App\Manager\Exception\NotFoundException;
use App\Manager\Query\Filter;
use App\Manager\Query\Query;
use App\Transformer\ActivityTransformer;
use App\Service\ParamConverter\Filter\Filters;
use Doctrine\ORM\Query\Expr\Comparison;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Rest\Route("/activity")
 */
class ActivityController
{
    /**
     * @var ActivityManager
     */
    private $activityManager;

    /**
     * @var ActivityTransformer
     */
    private $activityTransformer;

    /**
     * @var Security
     */
    private $security;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(
        ActivityManager $activityManager,
        ActivityTransformer $activityTransformer,
        Security $security,
        ValidatorInterface $validator
    ) {
        $this->activityManager = $activityManager;
        $this->activityTransformer = $activityTransformer;
        $this->security = $security;
        $this->validator = $validator;
    }

    /**
     * @Rest\Get()
     * @Rest\View()
     *
     * @Rest\QueryParam(name="page", requirements="[1-9]+\d*", default="1")
     * @Rest\QueryParam(name="limit", requirements="[1-9]+\d*", default="10")
     *
     * @Filters("filter", options={
     *     "customer": "activity.customer",
     *     "admin": "activity.admin"
     * })
     *
     * @param ParamFetcherInterface $paramFetcher
     * @param Filter $filter
     * @return ReadActivityCollectionDTO
     */
    public function getListAction(ParamFetcherInterface $paramFetcher, Filter $filter)
    {
        if (!$this->security->isGranted(User::ROLE_ADMIN)) {
            /** @var User $user */
            $user = $this->security->getUser();
            $filter->getWhere()->add(new Comparison('x.user', Comparison::EQ, $user->getId()));
        }

        $query = new Query(
            (int)$paramFetcher->get('page'),
            (int)$paramFetcher->get('limit'),
            $filter
        );
        $result = $this->activityManager->queryActivities($query);

        return $this->activityTransformer->resultToReadModelCollection($result);
    }

    /**
     * @Rest\Get("/{activityId}")
     * @Rest\View()
     *
     * @param int $activityId
     * @return ReadActivityDTO
     * @throws NotFoundException
     */
    public function getAction(int $activityId)
    {
        $activity = $this->activityManager->getActivity($activityId);

        if (!$this->security->isGranted('owner', $activity)) {
            throw new NotFoundHttpException('Activity not found');
        }

        return $this->activityTransformer->entityToReadModel($activity);
    }

    /**
     * @Rest\Post()
     * @Rest\View(statusCode=201)
     * @ParamConverter("model", converter="fos_rest.request_body" )
     *
     * @param WriteActivityDTO $model
     * @return ReadActivityDTO|View
     * @throws NotFoundException
     */
    public function createAction(WriteActivityDTO $model)
    {
        $validationGroups = ['Default'];

        if ($this->security->isGranted('ROLE_ADMIN')) {
            $validationGroups[] = 'AdminCreate';
        } else {
            /** @var User $user */
            $user = $this->security->getUser();
            $model->setUserId($user->getId());
        }

        $validationErrors = $this->validator->validate($model, null, $validationGroups);

        if (count($validationErrors)) {
            return new View($validationErrors, 400);
        }

        $activity = $this->activityTransformer->modelToEntity($model);
        $activity = $this->activityManager->createActivity($activity);
        $model = $this->activityTransformer->entityToReadModel($activity);

        return $model;
    }

    /**
     * @Rest\Put("/{activityId}")
     * @Rest\View()
     * @ParamConverter("model", converter="fos_rest.request_body" )
     *
     * @param int $activityId
     * @param WriteActivityDTO $model
     * @param ConstraintViolationListInterface $validationErrors
     * @return ReadActivityDTO|View
     * @throws NotFoundException
     */
    public function updateAction(int $activityId, WriteActivityDTO $model, ConstraintViolationListInterface $validationErrors)
    {
        $activity = $this->activityManager->getActivity($activityId);

        if (!$this->security->isGranted('owner', $activity)) {
            throw new NotFoundHttpException('Activity not found');
        }

        if (count($validationErrors)) {
            return new View($validationErrors, 400);
        }

        $model->setUserId($activity->getUser()->getId());

        $activity = $this->activityTransformer->modelToEntity($model, $activity);
        $activity = $this->activityManager->updateActivity($activity);
        $model = $this->activityTransformer->entityToReadModel($activity);

        return $model;
    }

    /**
     * @Rest\Delete("/{activityId}")
     * @Rest\View(statusCode=204)
     *
     * @param int $activityId
     * @throws NotFoundException
     */
    public function deleteAction(int $activityId)
    {
        $activity = $this->activityManager->getActivity($activityId);

        if (!$this->security->isGranted('owner', $activity)) {
            throw new NotFoundHttpException('Activity not found');
        }

        $this->activityManager->deleteActivity($activity);
    }
}
