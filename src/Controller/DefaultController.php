<?php declare(strict_types = 1);

namespace App\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;

class DefaultController
{
    /**
     * @Rest\View()
     * @Rest\Get("/")
     */
    public function indexAction()
    {
        return ['name' => 'Jogger API', 'version' => "1.0.0"];
    }
}
