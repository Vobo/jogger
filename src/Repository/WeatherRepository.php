<?php declare(strict_types=1);

namespace App\Repository;

use App\DTO\Weather\WeatherDTO;
use App\Service\Weather\ClientInterface;
use App\VO\Point;

class WeatherRepository
{
    /**
     * @var ClientInterface
     */
    private $weatherClient;

    public function __construct(ClientInterface $weatherClient)
    {
        $this->weatherClient = $weatherClient;
    }

    /**
     * @param \DateTime $date
     * @param Point $point
     * @return WeatherDTO
     */
    public function getWeather(\DateTime $date, Point $point): WeatherDTO
    {
        return $this->weatherClient->getHistoricByCoordinates($date, $point);
    }
}
