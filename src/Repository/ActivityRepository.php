<?php declare(strict_types=1);

namespace App\Repository;

use App\Entity\Activity;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ActivityRepository extends AbstractDbRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Activity::class);
    }

    /**
     * @param int $id
     * @return Activity|null
     */
    public function get(int $id): ?Activity
    {
        return $this->ormRepository->find($id);
    }
}
