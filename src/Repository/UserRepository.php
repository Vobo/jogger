<?php declare(strict_types=1);

namespace App\Repository;

use App\Entity\User;
use Symfony\Bridge\Doctrine\RegistryInterface;

class UserRepository extends AbstractDbRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * @param int $id
     * @return User|null
     */
    public function get(int $id): ?User
    {
        return $this->ormRepository->find($id);
    }
}
