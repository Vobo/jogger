<?php declare(strict_types=1);

namespace App\Repository;

use App\Manager\Query\Filter;
use App\Manager\Query\Query;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

abstract class AbstractDbRepository
{
    /**
     * @var ServiceEntityRepository
     */
    protected $ormRepository;

    public function __construct(RegistryInterface $registry, string $entityClass)
    {
        $this->ormRepository = new ServiceEntityRepository($registry, $entityClass);
    }

    /**
     * @param Query $query
     * @return array
     */
    public function query(Query $query): array
    {
        return $this
            ->createQueryBuilder($query->getFilter())
            ->setMaxResults($query->getSize())
            ->setFirstResult($query->getOffset())
            ->getQuery()
            ->getResult();
    }

    /**
     * @param Filter $filter
     * @return int
     */
    public function count(Filter $filter): int
    {
        return (int)$this
            ->createQueryBuilder($filter)
            ->select('count(x)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param Filter $filter
     * @return QueryBuilder
     */
    protected function createQueryBuilder(Filter $filter): QueryBuilder
    {
        $qb = $this->ormRepository->createQueryBuilder('x');

        if ($filter->getWhere()->count()) {
            $qb->andWhere($filter->getWhere());
        }

        if ($filter->getParameters()) {
            $qb->setParameters($filter->getParameters());
        }

        return $qb;
    }
}
