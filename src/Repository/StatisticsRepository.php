<?php declare(strict_types=1);

namespace App\Repository;

use App\DTO\Report\StatisticsDTO;
use App\Entity\Activity;
use App\Manager\Query\Filter;
use App\Manager\Query\Query;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\ArrayTransformerInterface;
use Symfony\Bridge\Doctrine\RegistryInterface;

class StatisticsRepository extends AbstractDbRepository
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ArrayTransformerInterface
     */
    private $arrayTransformer;

    public function __construct(
        RegistryInterface $registry,
        EntityManagerInterface $entityManager,
        ArrayTransformerInterface $arrayTransformer
    ) {
        parent::__construct($registry, Activity::class);
        $this->entityManager = $entityManager;
        $this->arrayTransformer = $arrayTransformer;
    }

    /**
     * @inheritdoc
     */
    public function query(Query $query): array
    {
        $result = $this
            ->createQueryBuilder($query->getFilter())
            ->resetDQLPart('select')

            ->addSelect('YEAR(x.date) as year')
            ->addSelect('WEEK(x.date) as week')
            ->addSelect('IDENTITY(x.user) as userId')
            ->addSelect('AVG(x.distance) as averageDistance')
            ->addSelect('AVG(x.time) as averageTime')

            ->addGroupBy('userId')
            ->addGroupBy('year')
            ->addGroupBy('week')

            ->addOrderBy('year', 'DESC')
            ->addOrderBy('week', 'DESC')
            ->addOrderBy('userId', 'ASC')

            ->setMaxResults($query->getSize())
            ->setFirstResult($query->getOffset())

            ->getQuery()
            ->getResult();

        return $this->arrayTransformer->fromArray(
            $result,
            'array<'.StatisticsDTO::class.'>'
        );
    }

    /**
     * @inheritdoc
     */
    public function count(Filter $filter): int
    {
        $query = <<<QUERY
            SELECT 
                COUNT(DISTINCT CONCAT(IDENTITY(x.user), YEARWEEK(x.date)))
            FROM App\\Entity\\Activity x
            WHERE %s
QUERY;

        $query = $this->entityManager->createQuery(sprintf(
            $query,
            $filter->getWhere()->count() ? (string)$filter->getWhere() : '1=1'
        ));

        if ($filter->getParameters()) {
            $query->setParameters($filter->getParameters());
        }

        return (int)$query->getSingleScalarResult();
    }
}
