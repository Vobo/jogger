<?php declare(strict_types=1);

namespace App\VO;

use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

class Point
{
    /**
     * @var float
     *
     * @JMS\Type("float")
     *
     * @Assert\NotBlank()
     * @Assert\Type("float")
     * @Assert\Range(min="-90", max="90")
     */
    private $latitude;

    /**
     * @var float
     *
     * @JMS\Type("float")
     *
     * @Assert\NotBlank()
     * @Assert\Type("float")
     * @Assert\Range(min="-180", max="180")
     */
    private $longitude;

    /**
     * @param float $latitude
     * @param float $longitude
     */
    public function __construct(float $latitude, float $longitude)
    {
        $this->latitude  = $latitude;
        $this->longitude = $longitude;
    }

    /**
     * @return float
     */
    public function getLatitude(): float
    {
        return $this->latitude;
    }

    /**
     * @return float
     */
    public function getLongitude(): float
    {
        return $this->longitude;
    }
}
