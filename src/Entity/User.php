<?php declare(strict_types=1);

namespace App\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class User extends BaseUser
{
    public const ROLE_ADMIN = 'ROLE_ADMIN';
    public const ROLE_MANAGER = 'ROLE_MANAGER';
    public const ROLE_CUSTOMER = 'ROLE_CUSTOMER';

    /**
     * @var int|null
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @return array
     */
    public function getRawRoles(): array
    {
        return $this->roles;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getMainRole(): string
    {
        if (count($this->roles) !== 1) {
            throw new \Exception('Unexpected roles present');
        }

        return reset($this->roles);
    }
}
