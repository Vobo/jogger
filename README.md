Jogger service
==============

##Task

Write a REST API that tracks jogging times of users

- API Users must be able to create an account and log in.
- All API calls must be authenticated.
- Implement at least three roles with different permission levels:
  - a regular user would only be able to CRUD on their owned records,
  - a user manager would be able to CRUD only users,
  - an admin would be able to CRUD all records and users.
- Each time entry when entered has:
  - date,
  - distance,
  - time,
  - location.
- Based on the provided date and location, API should connect to a weather API provider and get the weather conditions for the run, and store that with each run.
- The API must create a report on average speed & distance per week.
- The API must be able to return data in the JSON format.
- The API should provide filter capabilities for all endpoints that return a list of elements, as well should be able to support pagination.
- The API filtering should allow using parenthesis for defining operations precedence and use any combination of the available fields. The supported operations should at least include or, and, eq (equals), ne (not equals), gt (greater than), lt (lower than).
Example -> (date eq '2016-05-01') AND ((distance gt 20) OR (distance lt 10)).
- Write unit tests.

## Setup

### xdebug
Before build, run the commands below and update `.env`:

```
export CRH=$(ifconfig | grep "inet " | grep -Fv 127.0.0.1 | awk '{print $2}')
cp .env.dist .env
```

It will set CRH with the docker container IP. CRH is used as xdebug.remote_host (look at docker-compose.yml)

*phpStorm settings:*
* Port `9001`
* Server name and host name: `jogger.develop`
* Absolute path on the server: `/srv/www/service`

### docker
To start containers:

```
docker-compose build
docker-compose up -d
```

To remove docker containers:

```
docker-compose down
```

To see docker containers:

```
docker-compose ps
```

Create a new client:

```
php bin/console oauth:client:create --redirect-uri=http://jogger.develop --grant-type=password --grant-type=refresh_token
```

Create users:

```
php bin/console fos:user:create admin michal.czyzynski+admin@gmail.com admin
php bin/console fos:user:promote admin ROLE_ADMIN

php bin/console fos:user:create manager michal.czyzynski+manager@gmail.com manager
php bin/console fos:user:promote manager ROLE_MANAGER

php bin/console fos:user:create user michal.czyzynski+user@gmail.com user
php bin/console fos:user:promote manager ROLE_CUSTOMER
```

## Documentation

### Roles

Roles:
- ROLE_ADMIN - system admin, can do everything
- ROLE_MANAGER - user manager, has access to User endpoints
- ROLE_CUSTOMER - customer, can access his own user, Activity endpoints and reports

### OAuth

This system uses OAuth 2.0 standard for authentication.

To authenticate send following request:

```
POST jogger.develop/oauth/v2/token

{
    "grant_type": "password",
    "client_id": "{client id}",
    "client_secret": "{client secret}",
    "username": "{username}",
    "password": "{password}"
}
```

Params:
- client id: id returned by `oauth:client:create` console command
- client secret: secret returned by `oauth:client:create` console command
- username: `user`, `manager` or `admin`
- password: by default same as username

Example response:

```
{
    "access_token": "NWNmZDI5NDhhOWM1Y2IwMzM1ZTc5MjFkNWExMmM4YWFjYmRmYThhYzNmOWUyMzkzZjM4MGQ5N2ZmMWVlOGNjMQ",
    "expires_in": 86400,
    "token_type": "bearer",
    "scope": null,
    "refresh_token": "NTcxYjdkN2MyNTI3MzFkMzRiZTViMTFiMzQxZThjZGE0ODgyNmJhOWI0NzgxODlkZDk2NmEzMzI5ODMzN2RmMw"
}
```

To authenticate any further request to the api, add following header:

```
Authorization: Bearer {access_token}
```

Where `access_token` parameter is the value from previous response.

### Filters

Supported operators:
- eq - equals
- neq - not equals
- gt - greater than
- gte - greater than or equals
- lt - less than
- lte - less than or equal
- has - string contains

### Endpoints

#### User

Endpoints:
- GET /api/user - list all users
- GET /api/user/{userId} - get single user
- POST /api/user - create new user
- PUT /api/user/{userId} - update user
- DELETE /api/user/{userId} - delete user

Pagination - query params:
- page - results page
- size - number of results per page
- filter - filters as string

Supported filters:
- id - user id
- username - username used to log in
- email - user email
- role - filter using `has` operator, since system supports multiple roles per user, but I've limited it to a single role; could be refactored

#### Activity

Endpoints:
 - GET /api/activity - list all activities
 - GET /api/activity/{activityId} - get single activity
 - POST /api/activity - create new activity
 - PUT /api/activity/{activityId} - update activity
 - DELETE /api/activity/{activityId} - delete activity

Pagination - query params:
- page - results page
- size - number of results per page
- filter - filters as string

Supported filters:
- id - activity id
- user - (admin only) activity author
- date - date of activity
- distance - distance in meters
- time - time in seconds
- latitude - latitude
- longitude - longitude
- temperature - temperature in Celsius
- windSpeed - wind speed in meter/sec

#### Report

Endpoints
- GET /api/report/statistics - get average speed and time for user activities

Pagination - query params:
- page - results page
- size - number of results per page
- filter - filters as string

Supported filters:
- user - (admin only) activity author
- year - year
- week - week number
- distance - distance in meters
- time - time in seconds
- latitude - latitude
- longitude - longitude
- temperature - temperature in Celsius
- windSpeed - wind speed in meter/sec

### Things obviously missing:

While this project showcases some of my skills, there are things that I would like to see in it.
None of them were specifically mentioned in the task description, but seem like something you would
normally be inclined to have in your application. Yet, due to time constraints, they had to be skipped.

These things include:
- filtering:
    - sorting
    - limit operators availables per filter
    - support for statistical functions in reports
- documentation using swagger - request formats, response codes, response formats,
- user controller
    - change user password
    - reset password

The one thing I'd actually like to see more are unit tests. Application is covered with functional tests,
more complicated code also has unit tests, but still there are parts that aren't covered.
